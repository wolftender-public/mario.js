/*
 * Amazing file containing code controlling display in nw.js version of the app
 * Should not be included for browser releases
 */

var screenWrapper = undefined;

// initialize the nw.js stuff
if ( typeof nw !== 'undefined' )
{
    var win = nw.Window.get ( );
    win.setMinimumSize ( 800, 600 );
}

window.addEventListener ( 'load', function ( )
{
    screenWrapper = document.getElementById ( 'screenWrapper' );
    scale ( );
} );

window.addEventListener ( 'resize', function ( )
{
    scale ( );   
} );

function scale ( )
{
    var scale = Math.min 
    (
        window.innerWidth / 640,
        window.innerHeight / 480
    );

    var shiftX = ( window.innerWidth - scale * 640 ) / 2;
    var shiftY = ( window.innerHeight - scale * 480 ) / 2;

    if ( screenWrapper )
    {
        screenWrapper.style.transformOrigin = 'top left';
        screenWrapper.style.transform = 'translate(' + shiftX + 'px, ' + shiftY + 'px) scale(' + scale + ')';
    }
}