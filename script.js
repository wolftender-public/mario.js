/*
 * Mario: The javascript Mario (a.k.a. how long will it take for nintento to take it down)
 *
 * About: The script is pretty self-explanatory: it's just a mario game
 * Author: etheryt@protonmail.com
 * 
 * DISCLAIMER: EVERY ASSET IN THIS GAME BELONGS TO NINTENDO
 */

var animationList = ['axe','basement_goomba','beetle_l','beetle_r','bowser_idle_l','bowser_idle_r','bullet_bill_l','bullet_bill_r','castle_goomba','coin','fireball_down','fireball_l','fireball_r','fireball_up','fire_flower','goomba','green_koopa_l','green_koopa_r','green_plant_down','green_plant_up','hammerbro_l','hammerbro_r','icons','lakitu','lakitu_throw','launcher','life_mushroom','mario_idle_l','mario_idle_r','mario_walk_r','mario_walk_l','mario_jump_r','mario_jump_l','mushroom','parakoopa_l','parakoopa_r','platform','poison_mushroom','red_koopa_l','red_koopa_r','red_parakoopa_l','red_parakoopa_r','red_plant_down','red_plant_up','spiny_l','spiny_r','warp_d','warp_l','warp_r','warp_u','mario_idle_l_m','mario_idle_r_m','mario_walk_r_m','mario_walk_l_m','mario_jump_r_m','mario_jump_l_m','mario_dead','mario_crouch_m','mario_idle_l_f','mario_idle_r_f','mario_walk_r_f','mario_walk_l_f','mario_jump_r_f','mario_jump_l_f','mario_crouch_f','player_fireball','fireball_explosion','mario_fireball_r_f','mario_fireball_l_f','goomba_dead','basement_goomba_dead','castle_goomba_dead','red_shell_idle','red_shell','green_shell_idle','green_shell','hammer_idle_r','hammer_idle_l','hammer_l','hammer_r','black_shell_idle','black_shell','spiny_egg','coin_item','mario_warp','mario_warp_m','mario_warp_f','thwomp_idle','thwomp_angry','boo_idle_r','boo_idle_l','boo_active_r','boo_active_l','bowser_fall_l','bowser_fall_r','flag_pole','flag','mario_flag','mario_flag_m','mario_flag_f','castle','castle_s','big_castle','big_castle_s','toad_idle','pop','athletic_platform','smasher_base','smasher_down','smasher_up','smasher_idle_down','smasher_idle_up'];

/*
 * A little bit of tileset indexing
 * It will allow O(1) lookup time for some data
 */
var tilesetMask =
[
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
    0, 0, 2, 2, 2, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 3, 1, 1, 1, 1, 1, 1, 1, 0,
    0, 0, 0, 0, 0, 1, 1, 3, 3, 3, 1, 1, 1, 1, 1,
    1, 2, 2, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
    1, 1, 1
];

var tilesetAnimations =
[
    0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
    0, 0, 0, 0, 0, 2, 3, 4, 0, 0, 0, 0, 0, 0, 0, 
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
    0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 2,
    3, 4, 2, 3, 4, 1, 1, 5, 5, 5, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
    0, 0, 0
];

var spritefontData = {"0":0,"1":1,"2":2,"3":3,"4":4,"5":5,"6":6,"7":7,"8":8,"9":9,"a":10,"b":11,"c":12,"d":13,"e":14,"f":15,"g":16,"h":17,"i":18,"j":19,"k":20,"l":21,"m":22,"n":23,"o":24,"p":25,"q":26,"r":27,"s":28,"t":29,"u":30,"v":31,"w":32,"y":33,"z":34,"x":35,"!":36,"-":37};

// var soundList = ['bgm_bonus','bgm_castle','bgm_clear','bgm_dead','bgm_gameover','bgm_overworld','bgm_success','bgm_time','bgm_underground'];

var mario =
{
    // component registry
    components:
    {
        physics:
        {
            initialize: function ( entity )
            {
                entity.physics.velocity = { x: 0, y: 0 };
                console.log ( 'registered a new entity in physics system' );
            }
        },

        player:
        {
            initialize: function ( entity, data ) 
            { 
                entity.isPlayer = true;
                entity.jumpBoostTimeout = 0;
                entity.onGroundTimer = 0;
                entity.facing = 0;
                entity.isJumping = false;
                entity.powerUp = mario.powerUp.none;
                entity.renderOrder = 1; // put it on the priority layer
                entity.crouching = false;
                entity.fireballTimeout = 0;
                entity.lastFrameShot = false;
                entity.fireballAnimationTimer = 0;
                entity.invulnerability = 0;
                entity.stomps = 0;
                entity.isWarping = false;
                entity.deathVelocity = 9;
                entity.movementDisabled = false;
                entity.priorityPersistence = true;
                entity.blockBumped = false;

                mario.player = entity;
            },
            
            update: function ( entity, timestamp )
            {
                if ( entity.powerUp != mario.gamemode.powerup )
                    entity.powerUp = mario.gamemode.powerup;

                entity.stomps = 0;

                if ( entity.movementDisabled ) return;
                if ( entity.isLava ) mario.playerHit ( 999 );

                if ( entity.isWarping )
                {
                    entity.physics.disabled = true;
                    entity.invulnerability = 10;
                    entity.isWarping = false;
                    entity.renderOrder = -1;
                    return;
                }

                entity.renderOrder = 1;
                entity.physics.disabled = false;

                if ( entity.invulnerability > 0 )
                    entity.invulnerability--;

                if ( ( entity.position.y > 550 || entity.powerUp < 0 ) && !entity.deathTimer )
                {
                    mario.gamemode.powerup = 0;
                    mario.player = undefined;
                    entity.deathTimer = 1;
                    entity.physics.disabled = true;
                    mario.stopLevelMusic ( );
                    mario.playSound ( 'snd_deathtrigger' );
                    mario.gamemode.lives--;
                    mario.isPaused = true;

                    if ( entity.position.y > 550 ) entity.deathAnimation = false;
                    else entity.deathAnimation = true;
                }

                if ( entity.deathTimer )
                {
                    entity.animation = 'mario_dead';
                    entity.deathTimer++;
                    if ( entity.deathTimer == 25 ) mario.playSound ( 'snd_death' );
                    
                    if ( entity.deathTimer > 25 && entity.deathTimer < 40 && entity.deathAnimation ) entity.position.y -= 8;
                    else if ( entity.deathTimer > 40 )
                    {
                        if ( entity.deathVelocity < 20 ) entity.deathVelocity++;

                        if ( entity.position.y < 600 && entity.deathAnimation ) entity.position.y += entity.deathVelocity;
                        if ( entity.deathTimer > 100 && entity.position.y > 570 ) mario.progressionFail ( );
                    }

                    return;
                }

                if ( entity.powerUp > 0 )
                {
                    entity.dimensions.y = 64;
                    entity.origin.y = 48;
                    entity.bounding = { x: -13.75, y: -48, w: 27, h: 64 };
                }
                else
                {
                    entity.dimensions.y = 32;
                    entity.origin.y = 16;
                    entity.bounding = { x: -13.75, y: -16, w: 27, h: 32 };
                }

                if ( entity.jumpBoostTimeout > 0 ) entity.jumpBoostTimeout--;
                var friction = 1;
                
                if ( entity.physics.velocity.x > 0 )
                {
                    if ( entity.physics.velocity.x >= friction ) entity.physics.velocity.x -= friction;
                    else entity.physics.velocity.x = 0;
                }

                if ( entity.physics.velocity.x < 0 )
                {
                    if ( entity.physics.velocity.x <= -friction ) entity.physics.velocity.x += friction;
                    else entity.physics.velocity.x = 0;
                }

                var maxVelocity = 5;
                entity.animationSpeed = 20;

                if ( entity.fireballTimeout > 0 ) entity.fireballTimeout--;
                if ( mario.keyPressed.sprint ) 
                {
                    maxVelocity = 7;
                    entity.animationSpeed = 30;

                    if ( !entity.lastFrameShot && entity.fireballTimeout <= 0 && !entity.crouching && entity.powerUp == mario.powerUp.fireflower )
                    {
                        // shoot fireball
                        entity.fireballTimeout = 20;
                        var fireball = mario.spawnEntity ( entity.position.x, entity.position.y - 32, 40 ); // spawn fireball

                        entity.fireballAnimationTimer = 3;
                        mario.playSound ( 'snd_fireball' );

                        if ( entity.facing == 0 ) fireball.direction = 1;
                        else fireball.direction = -1;
                    }

                    entity.lastFrameShot = true;
                }
                else { entity.lastFrameShot = false; }

                if ( mario.keyPressed.down && entity.powerUp >= mario.powerUp.mushroom ) entity.crouching = true;
                else entity.crouching = false;

                if ( mario.keyPressed.left && !entity.crouching )
                {
                    if ( entity.physics.velocity.x > -maxVelocity ) entity.physics.velocity.x -= 2;
                    entity.facing = 1;
                }
                
                if ( mario.keyPressed.right && !entity.crouching )
                {
                    if ( entity.physics.velocity.x < maxVelocity ) entity.physics.velocity.x += 2;
                    entity.facing = 0;
                }

                if ( Math.abs ( entity.physics.velocity.x ) > maxVelocity )
                {
                    entity.physics.velocity.x -= Math.sign ( entity.physics.velocity.x ) * 0.5;
                }

                if ( entity.isJumping && entity.onGroundTimer > 2 ) 
                {
                    if ( mario.currentCombo > 0 ) mario.currentCombo = 0;
                    entity.isJumping = false;
                }

                if ( mario.keyPressed.up )
                {
                    if ( entity.onGroundTimer > 3 )
                    {
                        entity.jumpBoostTimeout = mario.jumpProperties.jumpBoostTimer;
                        entity.physics.velocity.y = -mario.jumpProperties.jumpHeight;
                        entity.onGroundTimer = 0;
                        entity.isJumping = true;

                        if ( entity.crouching ) entity.physics.velocity.y = -10;

                        mario.playSound ( 'snd_jump' );
                    }

                    if ( entity.jumpBoostTimeout > 0 )
                    {
                        entity.physics.velocity.y -= mario.jumpProperties.jumpBoost;
                    }
                }

                // mario.camera.x = Math.round ( entity.position.x ) - 320;

                if ( entity.facing == 0 ) 
                {
                    entity.animation = 'mario_idle_r';
                    if ( entity.physics.velocity.x > 0 ) entity.animation = 'mario_walk_r';
                    if ( entity.isJumping ) entity.animation = 'mario_jump_r';
                    if ( entity.fireballAnimationTimer > 0 ) entity.animation = 'mario_fireball_r';
                }
                else 
                {
                    entity.animation = 'mario_idle_l';
                    if ( entity.physics.velocity.x < 0 ) entity.animation = 'mario_walk_l';
                    if ( entity.isJumping ) entity.animation = 'mario_jump_l';
                    if ( entity.fireballAnimationTimer > 0 ) entity.animation = 'mario_fireball_l';
                }

                if ( entity.fireballAnimationTimer > 0 )
                    entity.fireballAnimationTimer--;

                if ( entity.crouching )
                {
                    entity.animation = 'mario_crouch';
                    entity.dimensions.y = 44;
                    entity.origin.y = 28;
                }

                if ( entity.powerUp == mario.powerUp.mushroom ) entity.animation = entity.animation + '_m';
                else if ( entity.powerUp == mario.powerUp.fireflower ) entity.animation = entity.animation + '_f';
            }
        },

        mushroom:
        {
            initialize: function ( entity, data ) 
            { 
                entity.runFunction = function ( )
                {
                    mario.playerPowerup ( mario.powerUp.mushroom );
                }
            },

            update: function ( entity, data )
            {
                mario.mushroomUpdate ( entity, data );
            }
        },

        life_mushroom:
        {
            initialize: function ( entity, data ) 
            { 
                entity.runFunction = function ( )
                {
                    mario.playSound ( 'snd_life' );
                    mario.gamemode.lives++;
                }
            },

            update: function ( entity, data )
            {
                mario.mushroomUpdate ( entity, data );
            }
        },

        poison_mushroom:
        {
            initialize: function ( entity, data ) 
            { 
                entity.runFunction = function ( )
                {
                    mario.playerHit ( 1 );
                }
            },

            update: function ( entity, data )
            {
                mario.mushroomUpdate ( entity, data );
            }
        },

        fire_flower:
        {
            initialize: function ( entity, data ) { },

            update: function ( entity, data )
            {
                var player = mario.player;
                if ( !player ) return;

                if ( mario.intersects ( entity.position.x, entity.position.y, 32, 32, player.position.x, player.position.y, player.dimensions.x, player.dimensions.y ) )
                {
                    mario.playerPowerup ( mario.powerUp.fireflower );
                    entity.dead = true;
                }
            }
        },

        coin:
        {
            initialize: function ( entity, data ) 
            { 
                entity.collected = false;
                entity.ignoreFireballs = entity.ignoreBump = true;

                entity.onShellHit = function ( shell )
                {
                    entity.collected = true;
                };
            },

            update: function ( entity, timestamp )
            {
                var player = mario.player;
                if ( !player ) return;

                mario.behaviourDamagableEnemy ( entity, timestamp );

                if ( mario.intersects ( entity.position.x, entity.position.y, 32, 32, player.position.x, player.position.y, player.dimensions.x, player.dimensions.y ) || entity.collected )
                {
                    mario.playCoinSound ( );
                    mario.gamemode.coins++;
                    mario.gamemode.score += 100;

                    if ( mario.gamemode.coins > 99 )
                    {
                        mario.playSound ( 'snd_life' );
                        mario.gamemode.lives++;
                        mario.gamemode.coins = 0;
                    }

                    entity.dead = true;
                }
            }
        },

        player_fireball:
        {
            initialize: function ( entity, data ) 
            { 
                if ( !entity.direction ) entity.direction = 0;
                entity.animationSpeed = 10;
            },

            update: function ( entity, timestamp )
            {
                if ( !entity.isFireball )
                {
                    entity.isFireball = true;
                    entity.physics.velocity.y = -5;
                }

                // on hit
                if ( entity.deathTimer )
                {
                    entity.deathTimer--;
                    entity.physics.disabled = true;
                    entity.animation = 'fireball_explosion';
                    entity.dimensions = { x: 32, y: 32 };
                    entity.animationSpeed = 20;
                    entity.origin = { y: 16, x: 16 };

                    if ( entity.deathTimer <= 0 )
                        entity.dead = true;

                    return;
                }

                if ( entity.position.y > 500 )
                    entity.dead = true;

                if ( Math.abs ( entity.physics.velocity.x ) < 9.9 && entity.activated )
                    entity.deathTimer = 6;

                entity.physics.velocity.x = Math.sign ( entity.direction ) * 10;
                entity.activated = true;

                if ( entity.fireballBounce )
                {
                    entity.physics.velocity.y = -14;
                    entity.fireballBounce = false;
                }
            }
        },

        goomba:
        {
            initialize: function ( entity, data )
            {
                if ( entity.animation == 'goomba' ) entity.goombaType = 0;
                else if ( entity.animation == 'basement_goomba' ) entity.goombaType = 1;
                else if ( entity.animation == 'castle_goomba' ) entity.goombaType = 3;

                entity.bounding = { x: -10, y: -10, w: 20, h: 26 };

                entity.onStomp = function ( )
                {
                    entity.deathTimer = 10;
                    // mario.gamemode.score += 100;
                    mario.giveScoreCombo ( entity.position.x, entity.position.y );
                    mario.playSound ( 'snd_stomp' );
                };

                entity.onShellHit = function ( )
                {
                    entity.knocked = true;
                    mario.giveScoreCombo ( entity.position.x, entity.position.y );
                    mario.playKickSound ( );
                };

                entity.onFireballHit = entity.onBlockHit = function ( )
                {
                    entity.knocked = true;
                    mario.giveScore ( 1, entity.position.x, entity.position.y );
                    mario.playKickSound ( );
                };
            },

            update: function ( entity, timestamp )
            {
                if ( entity.isLava ) 
                {
                    entity.knocked = true;
                    mario.playKickSound ( );
                }

                if ( entity.knocked ) return mario.behaviourKnockout ( entity, timestamp );

                if ( entity.deathTimer )
                {
                    entity.deathTimer--;
                    entity.physics.velocity.x = 0;
                    
                    if ( entity.goombaType == 0 ) entity.animation = 'goomba_dead';
                    else if ( entity.goombaType == 1 ) entity.animation = 'basement_goomba_dead';
                    else if ( entity.goombaType == 2 ) entity.animation = 'castle_goomba_dead';

                    if ( entity.deathTimer <= 0 )
                        entity.dead = true;

                    return;
                }

                mario.behaviourSimple ( entity, timestamp );
                
                if ( !mario.behaviourStompable ( entity, timestamp ) )
                {
                    mario.behaviourAggressive ( entity, timestamp );
                }

                mario.behaviourDamagableEnemy ( entity, timestamp );
            }
        },

        koopa:
        {
            initialize: function ( entity, data )
            {
                entity.bounding = { x: -10, y: -16, w: 20, h: 32 };
                entity.onGroundTimer = 0;

                entity.onStomp = function ( )
                {
                    mario.giveScoreCombo ( entity.position.x, entity.position.y );
                    mario.playSound ( 'snd_stomp' );

                    mario.spawnEntity ( entity.position.x, entity.position.y, 41 + entity.koopa.color );
                    entity.dead = true;                       
                }

                entity.onShellHit = function ( )
                {
                    entity.knocked = true;
                    mario.giveScoreCombo ( entity.position.x, entity.position.y );
                    mario.playKickSound ( );
                };

                entity.onFireballHit = entity.onBlockHit = function ( shell )
                {
                    mario.giveScore ( 1, entity.position.x, entity.position.y );
                    entity.knocked = true;
                    mario.playKickSound ( );
                };
            },

            update: function ( entity, timestamp )
            {
                if ( entity.isLava && !entity.knocked ) 
                {
                    entity.knocked = true;
                    mario.playKickSound ( );
                }

                if ( entity.knocked ) return mario.behaviourKnockout ( entity, timestamp );

                if ( entity.koopa.color == 0 )
                {
                    entity.prefix = 'green_';
                    mario.behaviourSimple ( entity, timestamp );
                }
                else if ( entity.koopa.color == 1 )
                {
                    entity.prefix = 'red_';
                    if ( entity.onGroundTimer > 2 ) mario.behaviourCareful ( entity, timestamp );
                    else mario.behaviourSimple ( entity, timestamp );
                }

                if ( !entity.direction )
                    entity.direction = -1;

                if ( entity.direction == -1 ) entity.animation = entity.prefix + 'koopa_l';
                else if ( entity.direction == 1 ) entity.animation = entity.prefix + 'koopa_r';

                if ( !mario.behaviourStompable ( entity, timestamp ) )
                {
                    mario.behaviourAggressive ( entity, timestamp );
                }

                mario.behaviourDamagableEnemy ( entity, timestamp );
            }
        },

        shell:
        {
            initialize: function ( entity, data )
            {
                if ( entity.animation == 'green_shell_idle' )
                    entity.shellType = 0;
                else if ( entity.animation == 'red_shell_idle' )
                    entity.shellType = 1;
                else if ( entity.animation == 'black_shell_idle' )
                    entity.shellType = 2;

                entity.stateTimer = 10;
                entity.isShell = true;

                entity.onStomp = function ( )
                {
                    if ( entity.stateTimer > 0 ) return;

                    if ( Math.abs ( entity.physics.velocity.x ) > 0 )
                    {
                        entity.physics.velocity.x = 0;
                        entity.stateTimer = 10;
                        mario.playSound ( 'snd_stomp' );
                    }
                    else
                    {
                        entity.physics.velocity.x = -Math.sign ( mario.player.position.x - entity.position.x ) * 10.5;
                        entity.stateTimer = 10;
                        mario.playKickSound ( );
                    }

                    mario.giveScoreCombo ( entity.position.x, entity.position.y );
                };

                entity.onBlockHit = function ( )
                {
                    // console.log ( 'rzer' );
                    entity.physics.velocity.y = -15;
                }
            },

            update: function ( entity, timestamp )
            {
                if ( entity.stateTimer > 0 ) entity.stateTimer--;
                entity.physics.bounceX = true;
                entity.bounding = { x: -16, y: -16, w: 32, h: 32 };
                mario.behaviourStompable ( entity, timestamp );

                if ( Math.abs ( entity.physics.velocity.x ) > 0 )
                {
                    entity.bounding = { x: -10, y: 0, w: 20, h: 20 };
                    mario.behaviourAggressive ( entity, timestamp );
                    entity.animation = 'shell';
                }
                else
                {
                    if ( !mario.player ) return;
                    var player = mario.player;

                    var playerRect = { x: player.position.x - player.origin.x, y: player.position.y - player.origin.y, w: player.dimensions.x, h: player.dimensions.y };

                    var bounding = mario.getEntityBounding ( entity );

                    if ( mario.intersects ( bounding.x + entity.position.x, bounding.y + entity.position.y, bounding.w, bounding.h, playerRect.x, playerRect.y, playerRect.w, playerRect.h ) && entity.stateTimer <= 0 )
                    {
                        entity.physics.velocity.x = -Math.sign ( mario.player.position.x - entity.position.x ) * 10.5;
                        player.invulnerability = 5;
                        entity.stateTimer = 10;
                        mario.playKickSound ( );
                    }

                    entity.animation = 'shell_idle';
                }

                if ( entity.shellType == 0 ) entity.animation = 'green_' + entity.animation;
                else if ( entity.shellType == 1 ) entity.animation = 'red_' + entity.animation;
                else if ( entity.shellType == 2 ) entity.animation = 'black_' + entity.animation;
                entity.bounding = { x: -16, y: -16, w: 32, h: 32 };
            }
        },

        bullet_bill:
        {
            initialize: function ( entity, data ) 
            {
                mario.playSound ( 'snd_cannon' );

                entity.startOffset = { x: mario.backgroundOffset.x, y: mario.backgroundOffset.y };
                entity.realPosition = { x: entity.position.x, y: entity.position.y };

                entity.onStomp = function ( )
                {
                    mario.playSound ( 'snd_stomp' );
                    entity.deathTimer = true;

                    mario.giveScoreCombo ( entity.position.x, entity.position.y );
                }
            },

            update: function ( entity, timestamp )
            {
                if ( !mario.isPaused && !entity.deathTimer )
                {
                    entity.position.x = entity.realPosition.x;
                    entity.position.y = entity.realPosition.y + mario.backgroundOffset.y - entity.startOffset.y;
                }

                if ( entity.deathTimer )
                {
                    entity.position.y += 12;
                    if ( entity.position.y > 500 ) entity.dead = true;

                    return;
                }

                if ( entity.bullet_bill.direction == 0 ) entity.bounding = { x: -12, y: -12, w: 28, h: 24 };
                else entity.bounding = { x: -16, y: -12, w: 28, h: 24 };

                if ( !mario.isPaused )
                {
                    if ( entity.bullet_bill.direction == 0 ) entity.realPosition.x -= 8;
                    else if ( entity.bullet_bill.direction == 1 ) entity.realPosition.x += 8;
                }

                if ( !mario.behaviourStompable ( entity, timestamp ) )
                {
                    mario.behaviourAggressive ( entity, timestamp );
                }
            }
        },

        launcher:
        {
            initialize: function ( entity, data )
            {
                // entity.launchTimer = 20;
            },

            update: function ( entity, timestamp )
            {
                if ( !mario.player ) return;
                // if ( entity.launchTimer > 0 ) entity.launchTimer--;
                entity.launchTimer = mario.animationTimer % 50;

                var playerDistance = Math.abs ( mario.player.position.x - entity.position.x );

                if ( entity.launchTimer <= 0 && ( playerDistance > 32 || playerDistance < 10 ) )
                {
                    // entity.launchTimer = 50; // 15 or 36
                    var sign = -Math.sign ( entity.position.x - mario.player.position.x );

                    var entID = 15;
                    if ( sign < 0 ) entID = 36;

                    mario.spawnEntity ( entity.position.x + sign * 25, entity.position.y, entID );
                }
            }
        },

        flying_koopa:
        {
            initialize: function ( entity, data )
            {
                entity.bounding = { x: -10, y: -16, w: 20, h: 32 };
                entity.color = entity.flying_koopa.color;

                // will not disable red koopa once it's offscreen
                if ( entity.color == 1 ) entity.priorityPersistence = true;

                entity.onStomp = function ( )
                {
                    mario.giveScoreCombo ( entity.position.x, entity.position.y );
                    mario.playSound ( 'snd_stomp' );

                    var newEntity = mario.spawnEntity ( entity.position.x, entity.position.y, 8 + ( entity.flying_koopa.color * 3 ) );
                    newEntity.direction = entity.direction;

                    entity.dead = true;                       
                }

                entity.onShellHit = function ( )
                {
                    entity.knocked = true;
                    mario.giveScoreCombo ( entity.position.x, entity.position.y );
                    mario.playKickSound ( );
                };

                entity.onFireballHit = function ( shell )
                {
                    entity.knocked = true;
                    mario.playKickSound ( );

                    mario.giveScore ( 1, entity.position.x, entity.position.y );
                };

                /*entity.onFireballHit = function ( fireball )
                {
                    entity.knocked = true;
                    mario.playKickSound ( );
                };*/

                entity.initialPosition = { x: entity.position.x, y: entity.position.y };
                entity.targetPosition = { x: entity.position.x, y: entity.position.y };

                if ( entity.color == 0 ) entity.targetPosition.y += ( 32 * 5.5 );
                else entity.targetPosition.x += ( 32 * 5.5 );

                if ( entity.targetPosition.y > 440 ) entity.targetPosition.y = 440;
                entity.movement = { x: 0, y: 0 };

                if ( entity.color == 0 ) entity.movement.y = 1;
                else entity.movement.x = 1;
            },

            update: function ( entity, timestamp )
            {
                if ( entity.isLava && !entity.knocked ) 
                {
                    entity.knocked = true;
                    mario.playKickSound ( );
                }

                if ( entity.knocked ) return mario.behaviourKnockout ( entity, timestamp );

                entity.direction = Math.sign ( entity.physics.velocity.x );

                if ( !entity.direction ) entity.direction = -1;
                if ( entity.direction == 0 ) entity.direction = -1;

                if ( entity.color == 0 )
                {
                    if ( entity.movement.y == 1 && entity.position.y >= entity.targetPosition.y ) entity.movement.y = -1;
                    else if ( entity.movement.y == -1 && entity.position.y <= entity.initialPosition.y ) entity.movement.y = 1;
                }
                else
                {
                    if ( entity.movement.x == 1 && entity.position.x >= entity.targetPosition.x ) entity.movement.x = -1;
                    else if ( entity.movement.x == -1 && entity.position.x <= entity.initialPosition.x ) entity.movement.x = 1;
                }

                entity.physics.velocity.x = entity.movement.x * 4;
                entity.physics.velocity.y = entity.movement.y * 4;

                // animations
                entity.animation = 'parakoopa';

                if ( entity.direction == -1 ) entity.animation = entity.animation + '_l';
                else entity.animation = entity.animation + '_r';

                if ( entity.color == 1 ) entity.animation = 'red_' + entity.animation;

                if ( !mario.behaviourStompable ( entity, timestamp ) )
                {
                    mario.behaviourAggressive ( entity, timestamp );
                }

                mario.behaviourDamagableEnemy ( entity, timestamp );
            }
        },

        jumping_koopa:
        {
            initialize: function ( entity, data )
            {
                entity.bounding = { x: -10, y: -16, w: 20, h: 32 };
                entity.jumpCooldown = 0;

                entity.onStomp = function ( )
                {
                    mario.giveScoreCombo ( entity.position.x, entity.position.y );
                    mario.playSound ( 'snd_stomp' );

                    var newEntity = mario.spawnEntity ( entity.position.x, entity.position.y, 8 + ( entity.jumping_koopa.color * 3 ) );
                    newEntity.direction = entity.direction;

                    entity.dead = true;
                }

                entity.onShellHit = function ( )
                {
                    entity.knocked = true;
                    mario.giveScoreCombo ( entity.position.x, entity.position.y );
                    mario.playKickSound ( );
                };

                entity.onFireballHit = function ( shell )
                {
                    entity.knocked = true;
                    mario.playKickSound ( );

                    mario.giveScore ( 1, entity.position.x, entity.position.y );
                };

                /*entity.onFireballHit = function ( fireball )
                {
                    entity.knocked = true;
                    mario.playKickSound ( );
                };*/

                entity.onGroundTimer = 0;
                entity.jumpBoostTimer = 0;

                if ( entity.jumping_koopa.color == 0 ) entity.prefix = '';
                else if ( entity.jumping_koopa.color == 1 ) entity.prefix = 'red_';
            },

            update: function ( entity, timestamp )
            {
                if ( entity.isLava ) 
                {
                    entity.knocked = true;
                    mario.playKickSound ( );
                }

                if ( entity.knocked ) return mario.behaviourKnockout ( entity, timestamp );
                if ( entity.jumpCooldown > 0 ) entity.jumpCooldown--;
                
                if ( entity.jumping_koopa.color == 0 ) { mario.behaviourSimple ( entity, timestamp ); }
                else
                {
                    if ( !entity.direction )
                    {
                        entity.direction = -1;
                        entity.physics.velocity.x = entity.direction * 4;
                    }

                    if ( entity.onGroundTimer >= 2 )
                    {
                        entity.direction = Math.sign ( mario.player.position.x - entity.position.x );
                        entity.physics.velocity.x = entity.direction * 4;
                    }
                }

                if ( entity.jumpBoostTimer > 0 )
                {
                    entity.physics.velocity.y -= 2;
                    entity.jumpBoostTimer--;
                }

                if ( entity.onGroundTimer >= 2 && entity.jumpCooldown <= 0 )
                {
                    entity.physics.velocity.y = -14;
                    entity.jumpBoostTimer = 4;
                    entity.jumpCooldown = 15;
                }

                if ( entity.direction == -1 ) entity.animation = entity.prefix + 'parakoopa_l';
                else if ( entity.direction == 1 ) entity.animation = entity.prefix + 'parakoopa_r';

                if ( !mario.behaviourStompable ( entity, timestamp ) )
                {
                    mario.behaviourAggressive ( entity, timestamp );
                }

                mario.behaviourDamagableEnemy ( entity, timestamp );
            }
        },

        piranha_green:
        {
            initialize: function ( entity, data ) { },

            update: function ( entity, timestamp )
            {
                mario.behaviourAggressive ( entity, timestamp );
            }
        },

        piranha_red:
        {
            initialize: function ( entity, data ) 
            { 
                entity.state = 0;
                entity.stateSwitchTimer = 30;

                entity.origin.x = 16;
                entity.position.x += 16;

                entity.statePosY = [ entity.position.y, entity.position.y ];

                if ( entity.piranha_red.direction == 0 ) entity.position.y += 48;
                else entity.position.y -= 48;

                entity.statePosY [0] = entity.position.y;
                entity.renderOrder = -1;

                entity.onShellHit = entity.onFireballHit = function ( shell ) 
                { 
                    mario.giveScore ( 1, entity.position.x, entity.position.y );
                    entity.dead = true; mario.playKickSound ( ); 
                }
                //entity.onFireballHit = function ( fireball ) { entity.dead = true; mario.playKickSound ( ); }

                if ( entity.piranha_red.direction == 0 ) entity.bounding = { x: -10, y: -20, w: 20, h: 32 }
                else if ( entity.piranha_red.direction == 1 ) entity.bounding = { x: -10, y: -10, w: 20, h: 32 }
            },

            update: function ( entity, timestamp )
            {
                if ( entity.stateSwitchTimer > 0 ) entity.stateSwitchTimer--;

                if ( entity.stateSwitchTimer <= 0 )
                {
                    if ( entity.state == 0 && mario.player )
                    {
                        var playerDist = Math.sqrt ( Math.pow ( mario.player.position.x - entity.position.x, 2 ) + Math.pow ( mario.player.position.y - entity.position.y, 2 ) );

                        if ( playerDist > 64 ) entity.state = 1;
                    }
                    else if ( entity.state == 1 ) entity.state = 0;

                    entity.stateSwitchTimer = 60;
                }

                if ( entity.position.y != entity.statePosY [entity.state] )
                {
                    var dir = Math.sign ( entity.statePosY [entity.state] - entity.position.y );
                    entity.position.y += dir * 3;
                }

                mario.behaviourAggressive ( entity, timestamp );
                mario.behaviourDamagableEnemy ( entity, timestamp );
            }
        },

        hammerbro:
        {
            initialize: function ( entity, data )
            {
                entity.bounding = { w: 20, h: 48, x: -10, y: -32 };
                entity.jumpedOnce = false;
                entity.jumpTimer = 115;
                entity.onGroundTimer = 0;
                entity.physIgnoreTimer = 0;
                entity.jumpBoostTimer = 0;
                entity.originalPosition = {x: entity.position.x, y: entity.position.y };
                entity.facing = -1;
                
                entity.onStomp = function ( )
                {
                    mario.giveScoreCombo ( entity.position.x, entity.position.y );
                    entity.knocked = true;
                    mario.playKickSound ( );
                }

                entity.onShellHit = function ( )
                {
                    entity.knocked = true;
                    mario.giveScoreCombo ( entity.position.x, entity.position.y );
                    mario.playKickSound ( );
                };

                entity.onFireballHit = entity.onBlockHit = function ( )
                {
                    mario.giveScore ( 1, entity.position.x, entity.position.y );
                    entity.knocked = true;
                    mario.playKickSound ( );
                }
            },

            update: function ( entity, timestamp )
            {
                if ( entity.isLava ) 
                {
                    entity.knocked = true;
                    mario.playKickSound ( );
                }

                if ( entity.knocked ) return mario.behaviourKnockout ( entity, timestamp );

                if ( entity.facing == -1 ) entity.animation = 'hammerbro_l';
                else if ( entity.facing == 1 ) entity.animation = 'hammerbro_r';

                if ( mario.player ) entity.facing = Math.sign ( mario.player.position.x - entity.position.x );

                entity.physics.bounceX = true;
                if ( entity.jumpTimer > 0 ) entity.jumpTimer--;

                if ( entity.jumpTimer >= entity.physIgnoreTimer && entity.jumpedOnce ) entity.physics.movable = false;
                else entity.physics.movable = true;

                if ( entity.jumpTimer >= entity.jumpBoostTimer && entity.jumpedOnce ) entity.physics.velocity.y -= 1.5;
 
                if ( entity.jumpTimer <= 0 && entity.onGroundTimer > 2 )
                {
                    // check if there are any tiles to fall on
                    var tileX = Math.floor ( entity.position.x / 32 );
                    var startTileY = Math.floor ( entity.position.y / 32 );
                    var emptyStreak = 0;

                    for ( var tileY = startTileY + 2; tileY < startTileY + 6; ++tileY )
                    {
                        if ( tileY > 14 ) { emptyStreak = 0; break; }
                        var index = mario.pointToIndex ( tileX, tileY );
                        var tile = mario.level.tiles [index][0];

                        var collisionMask = 0;
                        if ( tile > 0 ) collisionMask = tilesetMask [tile - 1]; 

                        if ( collisionMask == 0 ) emptyStreak++;
                        else break;
                    }

                    var canFall = ( emptyStreak >= 2 && emptyStreak <= 3 );
                    var currentBehaviour = 0;

                    if ( canFall ) currentBehaviour = Math.round ( Math.random ( ) );

                    entity.jumpTimer = 115;
                    if ( currentBehaviour == 0 ) 
                    {
                        entity.physIgnoreTimer = entity.jumpTimer - 20;
                        entity.physics.velocity.y = -20;
                    }
                    else entity.physIgnoreTimer = entity.jumpTimer - 12;
                    
                    entity.jumpBoostTimer = entity.jumpTimer - 2;
                    entity.jumpedOnce = true;
                }
                else
                {
                    // hammer throwing
                    if ( entity.jumpTimer == 100 || entity.jumpTimer == 75 || entity.jumpTimer == 50 || entity.jumpTimer == 25 && !mario.isPaused )
                    {
                        // throw hammer
                        var hammer = mario.spawnEntity ( entity.position.x, entity.position.y, 43 );
                        hammer.direction = entity.facing;

                        mario.playSound ( 'snd_hammer' );
                    }

                    // walking behaviour
                    if ( entity.jumpTimer % 15 == 0 )
                    {
                        if ( Math.round ( Math.random ( ) ) == 1 ) entity.physics.velocity.x = -entity.physics.velocity.x;
                    }

                    if ( Math.abs ( entity.physics.velocity.x ) < 1 ) entity.physics.velocity.x = -3;

                    if ( Math.abs ( entity.originalPosition.x - entity.position.x ) > 115 )
                    {
                        var dir = Math.sign ( entity.originalPosition.x - entity.position.x );
                        entity.physics.velocity.x = dir * 3;
                    }

                    if ( entity.jumpTimer >= entity.physIgnoreTimer && entity.jumpedOnce ) entity.physics.velocity.x = 0;
                }

                if ( !mario.behaviourStompable ( entity, timestamp ) )
                    mario.behaviourAggressive ( entity, timestamp );

                mario.behaviourDamagableEnemy ( entity, timestamp );
            }
        },

        hammer:
        {
            initialize: function ( entity, data ) 
            { 
                entity.throwTimer = 3;
                entity.horizontalVelocity = 4;
            },

            update: function ( entity, timestamp )
            {
                if ( entity.throwTimer > 0 )
                {
                    entity.throwTimer--;
                    entity.physics.velocity.y -= 8.5;
                }

                if ( !entity.direction )
                {
                    entity.direction = -1;
                    return;
                }

                if ( entity.direction == -1 ) entity.animation = 'hammer_l';
                else if ( entity.direction == 1 ) entity.animation = 'hammer_r';

                entity.physics.velocity.x = entity.direction * entity.horizontalVelocity;
                mario.behaviourAggressive ( entity, timestamp );
            }
        },

        spiny:
        {
            initialize: function ( entity, data )
            {
                entity.bounding = { x: -12, y: -10, w: 24, h: 26 };

                entity.onShellHit = function ( )
                {
                    entity.knocked = true;
                    mario.giveScoreCombo ( entity.position.x, entity.position.y );
                    mario.playKickSound ( );
                };

                entity.onFireballHit = entity.onBlockHit = function ( )
                {
                    mario.giveScore ( 1, entity.position.x, entity.position.y );
                    mario.playKickSound ( );
                    entity.knocked = true;
                }
            },

            update: function ( entity, data )
            {
                if ( entity.isLava ) 
                {
                    entity.knocked = true;
                    mario.playKickSound ( );
                }

                if ( entity.knocked ) return mario.behaviourKnockout ( entity, data );

                if ( !entity.direction )
                    entity.direction = -1;

                if ( entity.direction == -1 ) entity.animation = 'spiny_l';
                else if ( entity.direction == 1 ) entity.animation = 'spiny_r';

                mario.behaviourSimple ( entity, data );

                mario.behaviourDamagableEnemy ( entity, data );
                mario.behaviourAggressive ( entity, data );
            }
        },

        buzzy_beetle:
        {
            initialize: function ( entity, data )
            {
                entity.bounding = { x: -12, y: -10, w: 24, h: 26 };

                entity.onShellHit = function ( )
                {
                    entity.knocked = true;
                    mario.giveScoreCombo ( entity.position.x, entity.position.y );
                    mario.playKickSound ( );
                };

                entity.onBlockHit = function ( )
                {
                    mario.giveScore ( 1, entity.position.x, entity.position.y );
                    mario.playKickSound ( );
                    entity.knocked = true;
                }

                entity.onStomp = function ( )
                {
                    mario.giveScoreCombo ( entity.position.x, entity.position.y );
                    mario.spawnEntity ( entity.position.x, entity.position.y, 44 );
                    mario.playSound ( 'snd_stomp' );
                    entity.dead = true;
                }
            },
            
            update: function ( entity, timestamp )
            {
                if ( entity.isLava ) 
                {
                    entity.knocked = true;
                    mario.playKickSound ( );
                }

                if ( entity.knocked ) return mario.behaviourKnockout ( entity, timestamp );

                if ( !entity.direction )
                    entity.direction = -1;

                if ( entity.direction == -1 ) entity.animation = 'beetle_l';
                else if ( entity.direction == 1 ) entity.animation = 'beetle_r';

                mario.behaviourSimple ( entity, timestamp );

                if ( !mario.behaviourStompable ( entity, timestamp ) )
                {
                    mario.behaviourAggressive ( entity, timestamp );
                }

                mario.behaviourDamagableEnemy ( entity, timestamp );
            }
        },

        lakitu:
        {
            initialize: function ( entity, data )
            {
                entity.bounding = { x: -10, y: 0, w: 20, h: 25 };

                entity.onFireballHit = entity.onShellHit = entity.onStomp = function ( )
                {
                    mario.playKickSound ( );
                    entity.knocked = true;
                }

                entity.initialPosition = entity.position;
                entity.direction = -1;
                entity.throwTimer = 35;

                entity.prevCamX = mario.camera.x;

                entity.chase = false;
                entity.chaseDirection = 0;
            },

            update: function ( entity, timestamp )
            {
                if ( entity.knocked ) return mario.behaviourKnockout ( entity, timestamp );

                if ( !mario.player ) return;
                var player = mario.player;

                if ( entity.throwTimer > 0 ) entity.throwTimer--;
 
                var cameraVelocity = mario.camera.x - entity.prevCamX;
                entity.prevCamX = mario.camera.x;

                var screenLeft = mario.camera.x;
                var screenRight = screenLeft + 640;

                if ( !entity.chase )
                {
                    if ( Math.abs ( entity.position.x - player.position.x ) > 128 && player.physics.velocity.x != 0 )
                    {
                        entity.chase = true;
                        entity.chaseDirection = Math.sign ( player.physics.velocity.x );
                    }
                }
                else
                {
                    if ( entity.chaseDirection != Math.sign ( player.physics.velocity.x ) )
                        entity.chase = false;
                }

                // movement
                if ( entity.position.x < player.position.x - 128 ) entity.direction = 1;
                else if ( entity.position.x > player.position.x + 128 ) entity.direction = -1;

                if ( entity.position.x > player.position.x + 128 && cameraVelocity > 0 ) entity.direction = 0;
                if ( entity.position.x < player.position.x - 128 && cameraVelocity < 0 ) entity.direction = 0;

                entity.position.x += entity.direction * 3;
                if ( entity.chase ) entity.position.x += cameraVelocity;

                // throw spiny
                if ( entity.throwTimer <= 0 )
                {
                    entity.throwTimer = 65;
                    var egg = mario.spawnEntity ( entity.position.x, entity.position.y, 45 );

                    egg.physics.velocity.x = Math.sign ( player.position.x - entity.position.x ) * 3;
                }

                if ( !mario.behaviourStompable ( entity, timestamp ) )
                {
                    mario.behaviourAggressive ( entity, timestamp );
                }

                mario.behaviourDamagableEnemy ( entity, timestamp );
            }
        },

        spiny_egg:
        {
            initialize: function ( entity, data )
            {
                entity.onGroundTimer = 0;
            },

            update: function ( entity, data )
            {
                if ( entity.onGroundTimer > 2 )
                {
                    var spiny = mario.spawnEntity ( entity.position.x, entity.position.y, 26 );

                    spiny.direction = Math.sign ( entity.physics.velocity.x );
                    spiny.persistent = false;

                    entity.dead = true;
                }

                mario.behaviourAggressive ( entity, data );
            }
        },

        fireball:
        {
            initialize: function ( entity, data ) 
            { 
                mario.playSound ('snd_fire');
            },

            update: function ( entity, timestamp )
            {
                var direction = entity.fireball.direction;

                if ( direction < 0 ) entity.animation = 'fireball_l';
                else entity.animation = 'fireball_r';

                if ( !mario.isPaused )
                {
                    entity.position.x += Math.sign ( direction ) * 5;
                }

                entity.bounding = { x: -13, y: -12, w: 27, h: 25 };
                mario.behaviourAggressive ( entity, timestamp );
            }
        },

        fireball2:
        {
            initialize: function ( entity, data )
            {
                entity.targetPosition = entity.position.y;
                entity.prevY = entity.position.y;
                entity.jumpHeight = 496 - entity.targetPosition;
                entity.jumpSpeed = entity.jumpHeight / 6;

                entity.bounding = { x: -10, y: -10, w: 20, h: 20 };
            },

            update: function ( entity, timestamp )
            {
                if ( mario.isPaused ) return;
                entity.jumpTimer = mario.animationTimer % ( entity.jumpSpeed * 2 );
                
                var newPosition = 496 - ( entity.jumpHeight * Math.sin ( ( Math.PI / entity.jumpSpeed ) * entity.jumpTimer ) );
                if ( newPosition > 496 ) newPosition = 496;

                entity.position.y = newPosition;

                var direction = Math.sign ( entity.prevY - entity.position.y );

                if ( direction > 0 ) entity.animation = 'fireball_up';
                else entity.animation = 'fireball_down';

                entity.prevY = entity.position.y;
                mario.behaviourAggressive ( entity, timestamp );
            }
        },

        item_placeholder:
        {
            initialize: function ( entity, data )
            {
                entity.renderOrder = -1;
                entity.lifetime = 0;
                entity.coinAnimationFrame = 0;
                entity.initialized = false;
            },

            update: function ( entity, timestamp )
            {
                if ( !entity.initialized )
                {
                    if ( entity.item_placeholder.type == 0 ) 
                    {
                        entity.lifetime = 10;
                        mario.playCoinSound ( );

                        mario.gamemode.coins++;
                        mario.gamemode.score += 100;

                        if ( mario.gamemode.coins > 99 )
                        {
                            mario.playSound ( 'snd_life' );
                            mario.gamemode.lives++;
                            mario.gamemode.coins = 0;
                        }
                    }

                    if ( entity.item_placeholder.type >= 1 && entity.item_placeholder.type <= 4 )
                    {
                        entity.lifetime = 9;
                        mario.playSound ( 'snd_itemspawn' );

                        if ( entity.item_placeholder.type == 1 ) entity.animation = 'mushroom';
                        else if ( entity.item_placeholder.type == 2 ) entity.animation = 'life_mushroom';
                        else if ( entity.item_placeholder.type == 3 ) entity.animation = 'poison_mushroom';
                        else entity.animation = 'fire_flower';
                    }

                    if ( entity.item_placeholder.type == 6 )
                    {
                        entity.lifetime = 9;
                        mario.playSound ( 'snd_itemspawn' );
                        entity.animation = 'boo_idle_l';
                    }

                    entity.initialized = true;
                }

                entity.coinAnimationFrame += 4;

                if ( entity.lifetime >= 0 )
                {
                    entity.lifetime--;
                    if ( entity.lifetime == 0 )
                    {
                        if ( entity.item_placeholder.type >= 1 && entity.item_placeholder.type <= 4 )
                            mario.spawnEntity ( entity.position.x, entity.position.y, entity.item_placeholder.type );

                        if ( entity.item_placeholder.type == 6 )
                            mario.spawnEntity ( entity.position.x, entity.position.y, 38 );

                        entity.dead = true;
                        return;
                    }
                }

                if ( entity.item_placeholder.type == 0 )
                {
                    entity.position.y -= 8;
                    entity.dimensions.x = 40;
                    entity.origin.x = 20;
                    entity.animation = 'coin_item';
                    entity.animationTimer = -mario.animationTimer + entity.coinAnimationFrame;
                }
                else if ( entity.item_placeholder.type >= 1 && entity.item_placeholder.type <= 6 )
                {
                    entity.position.y -= 4;
                }

                if ( entity.dead ) entity.animation = undefined;
            }
        },

        warp:
        {
            initialize: function ( entity, data ) 
            { 
                if ( !entity.isWarping ) entity.isWarping = false;

                entity.animation = undefined;
                entity.enabled = false;
                entity.initialized = false;

                if ( entity.warp.direction == 0 || entity.warp.direction == 1 ) // up or down
                    entity.box = { x: 30, y: 27, w: 4, h: 5 };
                else
                    entity.box = { x: 30, y: 16, w: 2, h: 32 };
            },

            update: function ( entity, timestamp )
            {
                if ( !entity.initialized )
                {
                    if ( mario.world.warpIndex [entity.warpData.output] ) entity.enabled = true;
                    if ( entity.warpData.type != 0 ) entity.enabled = true;

                    entity.initialized = true;
                }

                if ( !entity.enabled ) return;
                if ( !mario.player ) return;
                var player = mario.player;

                var velocity = { x: 0, y: 0 };

                velocity.x = [0, 0, -1, 1][entity.warp.direction];
                velocity.y = [-1, 1, 0, 0][entity.warp.direction];

                if ( entity.isWarping )
                {
                    if ( entity.warp.direction == 0 || entity.warp.direction == 1 ) player.animation = 'mario_warp';
                    else if ( entity.warp.direction == 2 ) player.animation = 'mario_walk_l';
                    else player.animation = 'mario_walk_r';

                    if ( player.powerUp == mario.powerUp.mushroom ) player.animation = player.animation + '_m';
                    else if ( player.powerUp == mario.powerUp.fireflower ) player.animation = player.animation + '_f';

                    if ( entity.warpData.type == 1 )
                    {
                        if ( entity.warp.direction == 2 ) player.facing = 1;
                        else player.facing = 0;
                    }
                }

                if ( entity.isWarping && entity.warpData.type == 1 )
                {
                    mario.isPaused = true;
                    player.position.x += velocity.x * 2;
                    player.position.y += velocity.y * 2;

                    if ( entity.warp.direction == 0 && player.position.y < entity.position.y ) entity.isWarping = false;
                    if ( entity.warp.direction == 1 && player.position.y > entity.position.y ) entity.isWarping = false;
                    if ( entity.warp.direction == 2 && player.position.x < entity.position.x + 2 ) entity.isWarping = false;
                    if ( entity.warp.direction == 3 && player.position.x > entity.position.x - 2 ) entity.isWarping = false;

                    if ( !entity.isWarping ) mario.isPaused = false;

                    player.isWarping = true;
                }

                if ( entity.isWarping && entity.warpData.type == 0 )
                {
                    player.position.x += velocity.x * 2;
                    player.position.y += velocity.y * 2;

                    if ( entity.warp.direction == 0 || entity.warp.direction == 1 ) player.position.x = entity.position.x + 16;
                    else player.position.y = entity.position.y + 30;

                    if ( Math.abs ( player.position.x - entity.position.x ) > 64 || Math.abs ( player.position.y - entity.position.y ) > 64 )
                    {
                        console.log ( 'warping player to ' + entity.warpData.output );
                        mario.playSound ( 'snd_hit' );

                        mario.isPaused = true;
                        entity.isWarping = false;
                        
                        mario.warpPlayer ( entity.warpData.output );
                        mario.camera.x = mario.player.position.x - 320;
                        mario.world.warpIndex [entity.warpData.output].warpEntity.isWarping = true;
                    }

                    player.isWarping = true;
                }

                if ( !entity.isWarping && mario.intersects ( entity.position.x + entity.box.x, entity.position.y + entity.box.y, entity.box.w, entity.box.h, player.position.x, player.position.y, player.dimensions.x, player.dimensions.y ) && entity.warpData.type == 0 )
                {
                    if ( mario.keyPressed [['up','down','left','right'][entity.warp.direction]] ) 
                    {
                        entity.isWarping = true;
                        mario.playSound ( 'snd_hit' );
                    }
                }
            }
        },

        thwomp:
        {
            initialize: function ( entity, data )
            {
                entity.active = false;
                entity.startingPosition = entity.position;
                entity.onGroundTimer = 0;
                entity.activationTimeout = 0;

                entity.bounding = { x: -20, y: -32, w: 40, h: 64 };
            },

            update: function ( entity, timestamp )
            {
                var player = mario.player;
                if ( !player ) return;

                if ( entity.activationTimeout > 0 ) entity.activationTimeout--;

                if ( Math.abs ( player.position.x - entity.position.x ) < 64 + Math.abs ( player.physics.velocity.x * 3 ) && !entity.active && entity.activationTimeout <= 0 )
                {
                    entity.active = true;
                    entity.physics.gravity = true;
                    entity.physics.velocity.y += 12;
                    entity.onGroundTimer = 0;
                }

                if ( entity.active && entity.onGroundTimer == 1 ) mario.playSound ( 'snd_thwomp' );

                if ( entity.active && entity.physics.velocity.y >= 0 ) entity.animation = 'thwomp_angry';
                else entity.animation = 'thwomp_idle';

                if ( entity.active )
                {
                    if ( entity.position.y > entity.startingPosition.y && entity.onGroundTimer > 25 )
                    {
                        entity.physics.gravity = false;
                        entity.physics.velocity.y = -4;
                    }
                    
                    if ( entity.physics.velocity.y < 0 && entity.position.y <= entity.startingPosition.y )
                    {
                        entity.physics.gravity = false;
                        entity.active = false;
                        entity.physics.velocity.y = 0;
                        entity.position.y = entity.startingPosition.y;
                        entity.activationTimeout = 10;
                    }
                }

                mario.behaviourAggressive ( entity, timestamp );
            }
        },

        boo:
        {
            initialize: function ( entity ) 
            { 
                entity.facing = -1;
                entity.sinusoidBase = 0;
                entity.realPosition = entity.position;
                entity.bounding = { x: -10, y: -10, w: 20, h: 20 };
            },

            update: function ( entity, timestamp )
            {
                var player = mario.player;
                if ( !player ) return;

                entity.facing = -Math.sign ( entity.position.x - player.position.x );
                if ( entity.facing == 0 ) entity.facing = -1;

                // check if player is looking away
                if ( entity.facing == [1, -1][player.facing] )
                {
                    entity.animation = 'boo_active';

                    var dx = player.position.x - entity.position.x;
                    var dy = player.position.y - entity.position.y;
                    var d = Math.sqrt ( dx * dx + dy * dy );

                    var v = 3;
                    var vx = v * dx / d;
                    var vy = v * dy / d;

                    var initialY = entity.position.y;

                    entity.realPosition.x += vx;
                    entity.realPosition.y += vy;

                    entity.sinusoidBase += 1;

                    entity.position.x = entity.realPosition.x;
                    entity.position.y = entity.realPosition.y - Math.sin ( entity.sinusoidBase / 7 ) * 3;

                    if ( Math.abs ( entity.position.y - entity.initialY ) > 4 )
                    {
                        if ( initialY - entity.position.y < 0 ) entity.position.y = initialY - 4;
                        else entity.position.y = initialY + 4;
                    }
                }
                else
                {
                    entity.animation = 'boo_idle';
                    entity.sinusoidBase = 0;
                    entity.realPosition = entity.position;
                }

                if ( entity.facing == -1 ) entity.animation = entity.animation + '_l';
                else entity.animation = entity.animation + '_r';

                mario.behaviourAggressive ( entity, timestamp );
            }
        },

        bowser:
        {
            initialize: function ( entity, data )
            {
                entity.facing = -1;
                entity.health = 15;
                entity.direction = -1;
                entity.movementTimer = 15;
                entity.movementTimerBase = 11;
                entity.movementSpeed = 3;
                entity.originalPosition = { x: entity.position.x, y: entity.position.y };
                entity.onGroundTimer = 0;
                entity.movementPhase = 0;
                entity.movementPhaseTimer = 60;
                entity.jumpTimer = 75;
                entity.jumpBoostTimer = 0;
                entity.attackTimer = 33;
                entity.fireballTimer = 0;
                entity.hammerTimer = 20;
                entity.bounding = { x: -20, y: -40, w: 40, h: 56 };
                
                entity.isFalling = false;
                entity.isDying = false;

                entity.onFireballHit = function ( )
                {
                    entity.health--;

                    if ( entity.health <= 0 ) 
                    {
                        mario.playKickSound ( );
                        mario.giveScore ( 7, entity.position.x, entity.position.y );
                    }
                }
            },

            update: function ( entity, timestamp )
            {
                if ( entity.isLava ) entity.health = 0;
                if ( entity.health <= 0 ) return mario.behaviourKnockout ( entity, timestamp );

                var player = mario.player;
                if ( !player ) return;

                if ( entity.isFalling )
                {
                    if ( entity.facing == -1 ) entity.animation = 'bowser_fall_l';
                    else entity.animation = 'bowser_fall_r';

                    entity.physics.disabled = true;

                    if ( entity.isDying )
                    {
                        entity.position.y += 7;
                        entity.renderOrder = -1;
                    }

                    return;
                }
 
                entity.facing = -Math.sign ( entity.position.x - player.position.x );
                if ( entity.facing == 0 ) entity.facing = -1;

                entity.animation = 'bowser_idle';

                if ( entity.movementPhaseTimer > 0 ) entity.movementPhaseTimer--;
                else
                {
                    var currentPhase = entity.movementPhase;

                    entity.movementPhaseTimer = Math.floor ( 60 + Math.random ( ) * 7 );
                    entity.movementPhase = Math.floor ( Math.random ( ) * 2 );

                    if ( currentPhase == entity.movementPhase )
                        entity.movementPhase = ( entity.movementPhase + 1 ) % 3;

                    console.log ( 'new movement phase: ' + entity.movementPhase );
                }

                if ( entity.attackTimer > 0 ) entity.attackTimer--;
                else
                {
                    entity.attackTimer = 75;

                    var attackType = Math.floor ( Math.random (  ) * 2 );

                    if ( attackType == 0 ) entity.hammerTimer = 20;
                    else entity.fireballTimer = 25;
                }

                if ( entity.fireballTimer > 0 )
                {
                    entity.fireballTimer--;
                    entity.movementPhase = 1;
                   
                    if ( entity.fireballTimer == 20 )
                    {
                        var yPos = entity.position.y - [45, 0][Math.floor ( Math.random ( ) * 2 )];
                        var fireball = mario.spawnEntity ( entity.position.x, yPos, 23 );
                        fireball.fireball.direction = entity.facing;
                    }
                }

                if ( entity.hammerTimer > 0 )
                {
                    entity.hammerTimer--;

                    if ( entity.hammerTimer % 3 == 0 )
                    {
                        var hammer = mario.spawnEntity ( entity.position.x, entity.position.y, 43 );
                        hammer.direction = entity.facing;
                        hammer.horizontalVelocity = 8;
                    }
                }

                // movement
                if ( entity.movementPhase == 0 )
                {
                    entity.movementSpeed = 3;

                    if ( entity.movementTimer > 0 ) entity.movementTimer--;
                    else
                    {
                        if ( entity.direction == 0 ) entity.direction = -1;

                        entity.direction = -entity.direction;
                        entity.movementTimer = Math.floor ( entity.movementTimerBase + Math.random ( ) * 15 );
                    }

                    if ( Math.abs ( entity.position.x - entity.originalPosition.x ) > 115 )
                    {
                        entity.direction = Math.sign ( entity.position.x - entity.originalPosition.x );
                    }
                }
                else if ( entity.movementPhase == 1 )
                {
                    entity.movementSpeed = 4;
                    entity.direction = Math.sign ( player.position.x + 180 - entity.position.x );

                    if ( Math.abs ( player.position.x + 180 - entity.position.x ) < 16 ) entity.direction = 0;
                }
                else if ( entity.movementPhase == 2 )
                {
                    if ( entity.movementPhaseTimer > 40 ) entity.movementPhaseTimer = 40;

                    entity.movementSpeed = 3;
                    entity.direction = Math.sign ( player.position.x - entity.position.x );
                }

                // jumping
                if ( entity.position.y > player.position.y + 64 && entity.onGroundTimer > 1 )
                    entity.physics.velocity.y -= 17;

                if ( entity.jumpTimer > 0 ) entity.jumpTimer--;
                else
                {
                    entity.jumpTimer = 70 + Math.floor ( Math.random ( ) * 10 );

                    if ( entity.onGroundTimer > 1 )
                    {
                        var isHighJump = [false, false, true][Math.floor ( Math.random ( ) * 3 )]

                        entity.movementPhase = 2;
                        entity.physics.velocity.x = -15;
                        entity.movementPhaseTimer = 30;

                        if ( isHighJump )
                        {
                            entity.physics.velocity.y -= 15;
                            entity.jumpBoostTimer = 3;
                        }
                        else
                        {
                            entity.physics.velocity.y -= 12;
                            entity.jumpBoostTimer = 1;
                        }
                    }
                }

                if ( entity.jumpBoostTimer > 0 )
                {
                    entity.jumpBoostTimer--;
                    entity.physics.velocity.y -= 5.5;
                }

                if ( entity.onGroundTimer > 1 && entity.originalPosition.x > entity.position.x + 150 )
                    entity.direction = 1;

                entity.physics.velocity.x = entity.direction * entity.movementSpeed;

                if ( entity.facing == -1 ) entity.animation = entity.animation + '_l';
                else entity.animation = entity.animation + '_r';

                mario.behaviourAggressive ( entity, timestamp );
                mario.behaviourDamagableEnemy ( entity, timestamp );
            }
        },

        axe:
        {
            initialize: function ( entity, data )
            {
                entity.bridge = [];
                entity.activatedOnce = false;
                entity.activatedTimer = 0;

                var tx = Math.floor ( entity.position.x / 32 ) - 1;
                var ty = Math.floor ( entity.position.y / 32 ) + 2;

                var index = mario.pointToIndex ( tx, ty );

                if ( !mario.level.tiles [index] ) return;

                while ( mario.level.tiles [index][0] == 30 )
                {
                    entity.bridge.push ( index );

                    tx--;
                    index = mario.pointToIndex ( tx, ty );
                }
            },

            update: function ( entity, timestamp )
            {
                var player = mario.player;
                if ( !player ) return;

                if ( player.position.x > entity.position.x ) player.position.x = entity.position.x;

                if ( mario.intersects ( entity.position.x + 10, entity.position.y, 22, 32, player.position.x, player.position.y, player.dimensions.x, player.dimensions.y ) || entity.collected )
                {
                    if ( !entity.activatedOnce ) mario.giveScore ( 7, entity.position.x, entity.position.y );

                    entity.activatedOnce = true;
                    mario.playSound ( 'snd_bridge' );
                    mario.stopLevelMusic ( );

                    mario.destroyAllProjectiles ( );

                    for ( var i = 0; i < mario.entities.length; ++i )
                    {
                        var ent = mario.entities [i];

                        if ( ent.type == 'bowser' )
                            ent.isFalling = true;
                    }

                    mario.player.movementDisabled = true;
                    mario.player.physics.velocity = { x: 0, y: 0 };

                    mario.player.animation = 'mario_warp';
                    if ( mario.player.powerUp == 1 ) mario.player.animation = mario.player.animation + '_m';
                    else if ( mario.player.powerUp == 2 ) mario.player.animation = mario.player.animation + '_f';
                }

                if ( entity.activatedOnce )
                {
                    mario.player.position.x = entity.position.x;
                    mario.player.position.y = entity.position.y;
                    entity.animation = undefined;

                    mario.player.invulnerability = 9999;

                    if ( entity.activatedTimer % 2 == 0 )
                    {
                        var index = entity.activatedTimer / 2;

                        if ( index >= entity.bridge.length )
                        {
                            entity.dead = true;
                            mario.playSound ( 'snd_fall' );

                            for ( var i = 0; i < mario.entities.length; ++i )
                            {
                                var ent = mario.entities [i];

                                if ( ent.type == 'bowser' )
                                    ent.isDying = true;
                            }

                            setTimeout ( function ( )
                            {
                                mario.player.physics.velocity.x = 5;

                                mario.player.animation = 'mario_walk_r';
                                if ( mario.player.powerUp == 1 ) mario.player.animation = mario.player.animation + '_m';
                                else if ( mario.player.powerUp == 2 ) mario.player.animation = mario.player.animation + '_f';
                            }, 500 );

                            setTimeout ( function ( )
                            {
                                mario.playSound ( 'bgm_success' );
                            }, 900 );
                        }

                        mario.destroyBlock ( entity.bridge [index], 1 );
                    }

                    entity.activatedTimer++;
                }
            }
        },

        flagpole:
        {
            initialize: function ( entity, data ) 
            { 
                entity.flag = mario.spawnEntity ( entity.position.x + 18, entity.position.y - 285, 48 );
                entity.flag.renderOrder = 0;
                entity.animationPhase = 0;
                entity.victoryTimer = 0;
                entity.victory = false;
            },

            update: function ( entity, timestamp )
            {
                var player = mario.player;
                if ( !player ) return;

                if ( player.position.x > entity.position.x - 10 && !entity.victory ) 
                    player.position.x = entity.position.x - 10;

                var tx = Math.floor ( entity.position.x / 32 );
                var ty = Math.floor ( entity.position.y / 32 );

                var index = mario.pointToIndex ( tx, ty );

                mario.level.tiles [index][0] = 8;

                var bounding = { x: entity.position.x + 10, y: entity.position.y - 290, w: 12, h: 290 };

                if ( !entity.victory )
                {
                    if ( mario.intersects ( player.position.x, player.position.y, player.dimensions.x, player.dimensions.x, bounding.x, bounding.y, bounding.w, bounding.h ) )
                    {
                        player.movementDisabled = true;
                        player.physics.velocity = { x: 0, y: 0 };
                        player.position.x = entity.position.x - 10;
                        player.physics.gravity = false;

                        mario.destroyAllEnemies ( ); // kill everything
                    
                        player.animation = 'mario_flag';
                        if ( player.powerUp == 1 ) player.animation = player.animation + '_m';
                        else if ( player.powerUp == 2 ) player.animation = player.animation + '_f';

                        mario.stopLevelMusic ( );
                        mario.playSound ( 'bgm_clear' );
                        entity.victory = true;
                    }
                }
                else
                {
                    if ( entity.animationPhase == 0 )
                    {
                        if ( entity.flag.position.y < entity.position.y - 35 )
                            entity.flag.position.y += 6;

                        if ( player.position.y < entity.position.y - 35 ) 
                            player.position.y += 7;

                        if ( player.position.y >= entity.position.y - 35 && entity.flag.position.y >= entity.position.y - 35 )
                            entity.animationPhase++;
                    }
                    else if ( entity.animationPhase == 1 )
                    {
                        player.physics.gravity = true;
                        player.physics.velocity.x = 3;

                        player.animation = 'mario_walk_r';
                        if ( player.powerUp == 1 ) player.animation = player.animation + '_m';
                        else if ( player.powerUp == 2 ) player.animation = player.animation + '_f';

                        if ( player.position.x > entity.position.x + 250 )
                            entity.animationPhase++;
                    }
                    else
                    {
                        player.physics.velocity = { x: 0, y: 0 };

                        entity.victoryTimer++;
                        if ( entity.victoryTimer > 150 ) mario.progressionSuccess ( );

                        player.animation = 'mario_warp';
                        if ( player.powerUp == 1 ) player.animation = player.animation + '_m';
                        else if ( player.powerUp == 2 ) player.animation = player.animation + '_f';
                    }
                }
            }
        },

        falling_platform:
        {
            initialize: function ( entity, data )
            {
                entity.falling = false;
                entity.fallTimer = 0;
            },

            update: function ( entity, timestamp )
            {
                var player = mario.player;
                if ( !player ) return;

                if ( mario.intersects ( player.position.x - 16, player.position.y - 16, 32, 32, entity.position.x - 32, entity.position.y - 18, 64, 5 ) )
                    entity.falling = true;

                if ( entity.falling ) entity.fallTimer++;
                if ( entity.fallTimer > 5 ) entity.physics.velocity.y += 0.6;

                if ( entity.position.y > 500 && mario.intersects ( player.position.x - 16, player.position.y - 16, 32, 32, entity.position.x - 32, entity.position.y - 18, 64, 5 ) ) 
                {
                    entity.dead = true;
                    player.physics.velocity.y = 20;
                }
            }
        },

        moving_platform:
        {
            initialize: function ( entity, data )
            {
                entity.priorityPersistence = true;

                if ( entity.moving_platform.direction == 0 ) entity.direction = [0, 1];
                else if ( entity.moving_platform.direction == 1 ) entity.direction = [0, -1];
                else if ( entity.moving_platform.direction == 2 ) entity.direction = [1, 0];
                else entity.direction = [-1, 0];

                entity.originalDirection = [ entity.direction [0], entity.direction [1] ];
                entity.originalPosition = { x: entity.position.x, y: entity.position.y };
            },

            update: function ( entity, timestamp )
            {
                entity.physics.velocity.x = entity.direction [0] * 5;
                entity.physics.velocity.y = entity.direction [1] * 4;

                if ( Math.abs ( entity.position.x - entity.originalPosition.x ) > 320 && entity.direction [0] == entity.originalDirection [0] ) 
                    entity.direction [0] = -entity.direction [0];

                if ( Math.sign ( entity.position.x - entity.originalPosition.x ) == -entity.originalDirection [0] && entity.originalDirection [0] != entity.direction [0] )
                    entity.direction [0] = -entity.direction [0];

                if ( entity.position.y > 490 ) entity.position.y = -10;
                if ( entity.position.y < -10 ) entity.position.y = 490;
            }
        },

        merryboo:
        {
            initialize: function ( entity, data )
            {
                entity.animation = undefined;
                entity.angle = 0;
                entity.radius = 150;
                entity.angularVelocity = Math.PI / 120;

                entity.priorityPersistence = true;

                entity.boos = [ ];

                for ( var i = 0; i < 12; ++i )
                {
                    if ( i != 5 ) entity.boos.push ( mario.spawnEntity ( entity.position.x, entity.position.y, 56 ) );
                    else entity.boos.push ( undefined );
                }

                this.updatePositions ( entity );
            },

            updatePositions: function ( entity )
            {
                for ( var i = 0; i < 12; ++i )
                {
                    var currentAngle = ( i * ( Math.PI / 6 ) ) + entity.angle;
                    var boo = entity.boos [i];

                    if ( !boo ) continue;

                    boo.position.x = entity.position.x + Math.sin ( currentAngle ) * entity.radius;
                    boo.position.y = entity.position.y + Math.cos ( currentAngle ) * entity.radius;
                }
            },

            update: function ( entity, timestamp )
            {
                entity.animation = undefined;
                entity.angle += entity.angularVelocity;

                if ( !mario.isPaused ) this.updatePositions ( entity );
            }
        },

        merryboo_boo:
        {
            initialize: function ( entity, data ) 
            { 
                entity.bounding = { x: -8, y: -8, w: 16, h: 16 };
            },

            update: function ( entity, timestamp )
            {
                mario.behaviourAggressive ( entity, timestamp );
            }
        },

        flamespinner:
        {
            initialize: function ( entity, data )
            {
                entity.animation = undefined;

                entity.angularVelocity = Math.PI / 40 * entity.flamespinner.direction;
                entity.angle = entity.angularVelocity * mario.animationTimer;

                entity.priorityPersistence = true;
                entity.flames = [];

                for ( var i = 0; i < 7; ++i )
                    entity.flames.push ( mario.spawnEntity ( entity.position.x, entity.position.y, 58 ) );

                this.updatePositions ( entity );
            },

            updatePositions: function ( entity )
            {
                for ( var i = 0; i < entity.flames.length; ++i )
                {
                    var radius = 18 * i;
                    var flame = entity.flames [i];

                    flame.position.x = entity.position.x + Math.sin ( entity.angle ) * radius;
                    flame.position.y = entity.position.y + Math.cos ( entity.angle ) * radius;
                }
            },

            update: function ( entity, timestamp )
            {
                var tx = Math.floor ( entity.position.x / 32 );
                var ty = Math.floor ( entity.position.y / 32 );

                mario.level.tiles [mario.pointToIndex ( tx, ty )][0] = 29;

                entity.animation = undefined;
                entity.angle += entity.angularVelocity;

                if ( !mario.isPaused ) this.updatePositions ( entity );
            }
        },

        spinnerflame:
        {
            initialize: function ( entity, data ) 
            { 
                entity.bounding = { x: -2, y: -2, w: 4, h: 4 };
            },

            update: function ( entity, timestamp )
            {
                mario.behaviourAggressive ( entity, timestamp );
            }
        },

        cameralock:
        {
            initialize: function ( entity, data )
            {
                entity.animation = undefined;
            },

            update: function ( entity, timestamp )
            {
                mario.setCameraLock ( entity.position.x - 16 );
            }
        },

        toad:
        {
            initialize: function ( entity, data ) 
            { 
                entity.triggered = false;
                entity.text = [ "congratulations mario!", "thank you for testing all the courses in this world", "but the princess is in another castle" ];

                entity.triggerTimer = 0;
            },

            update: function ( entity, timestamp )
            {
                if ( entity.triggered )
                {
                    mario.player.animation = 'mario_idle_r';
                    if ( mario.player.powerUp == 1 ) mario.player.animation = mario.player.animation + '_m';
                    else if ( mario.player.powerUp == 2 ) mario.player.animation = mario.player.animation + '_f';

                    entity.triggerTimer++;
                    mario.player.physics.velocity = { x: 0, y: 0 };

                    if ( entity.triggerTimer == 30 ) mario.spawnEntity ( mario.player.position.x, 150, 62 ).text = entity.text [0];
                    if ( entity.triggerTimer == 75 ) mario.spawnEntity ( mario.player.position.x, 200, 62 ).text = entity.text [1];
                    if ( entity.triggerTimer == 105 ) mario.spawnEntity ( mario.player.position.x, 250, 62 ).text = entity.text [2];
                    if ( entity.triggerTimer == 230 ) mario.progressionSuccess ( );

                    return;
                }

                if ( Math.abs ( mario.player.position.x - entity.position.x ) <= 196 )
                    entity.triggered = true;
            }
        },

        text:
        {
            initialize: function ( entity, data )
            {
                entity.renderOrder = 1;
                entity.text = "";
                entity.animation = undefined;

                entity.onRender = function ( )
                {
                    var textWidth = 0;

                    for ( var i = 0; i < entity.text.length; ++i )
                    {
                        var ch = entity.text [i];

                        if ( ch.charCodeAt ( 0 ) == 32 ) textWidth += 8;
                        else textWidth += 17;
                    }

                    var posx = entity.position.x - textWidth / 2 - mario.camera.x;
                    var posy = entity.position.y;

                    mario.renderSpriteFontText ( posx, posy, entity.text );
                }
            },

            update: function ( entity, timestamp ) { }
        },

        pop:
        {
            initialize: function ( entity, data )
            {
                entity.lifetime = 0;
                entity.animation = 'pop';

                if ( Math.abs ( entity.position.x - mario.player.position.x ) < 320 )
                    mario.playKickSound ( );
            },

            update: function ( entity, timestamp )
            {
                entity.lifetime++;
                if ( entity.lifetime > 9 ) entity.dead = true;
            }
        },

        athletic_platform:
        {
            initialize: function ( entity, data )
            {
                entity.anchor = { x: entity.position.x, y: entity.position.y };
                entity.priorityPersistence = true;

                entity.radius = 150;
                entity.angularVelocity = Math.PI / 55;
                entity.angle = entity.angularVelocity * mario.animationTimer;

                entity.position.y -= entity.radius;
            },

            update: function ( entity, timestamp )
            {
                if ( !mario.isPaused ) entity.angle += entity.angularVelocity;

                var newX = entity.anchor.x + Math.sin ( entity.angle ) * entity.radius;
                var newY = entity.anchor.y + Math.cos ( entity.angle ) * entity.radius;

                entity.desiredPosition = { x: newX, y: newY };
                var delta = { x: newX - entity.position.x, y: newY - entity.position.y };

                if ( delta.x * delta.x + delta.y * delta.y < 96 * 96 )
                {
                    // flooring the velocity prevents strange things from happening
                    // besides, it looks smooth anyways on 30 fps so what's the point
                    entity.physics.velocity.x = Math.floor ( delta.x );
                    entity.physics.velocity.y = Math.floor ( delta.y );
                }
                else
                {
                    entity.position.x = newX;
                    entity.position.y = newY;
                }
            }
        },

        smasher_base:
        {
            initialize: function ( entity, data )
            {
                entity.animation = 'smasher_base';
                entity.retracted = false;

                entity.origin.x = 64;
                entity.origin.y = 16;

                entity.dimensions.x = 128;
                entity.dimensions.y = 32;

                if ( entity.smasher_base.type == -1 ) // smasher type = up
                {
                    entity.smasher = mario.spawnEntity ( entity.position.x, entity.position.y - 45, 67 );
                }
                else if ( entity.smasher_base.type == 1 ) // smasher type = down
                {
                    entity.smasher = mario.spawnEntity ( entity.position.x, entity.position.y + 75, 68 );
                }

                entity.smasherInitialY = entity.smasher.position.y;
            },

            update: function ( entity, timestamp )
            {
                var timer = mario.animationTimer % 190;

                if ( Math.abs ( entity.smasher.position.y - entity.smasherInitialY ) > 360 )
                    entity.smasher.position.y = entity.smasherInitialY + Math.sign ( entity.smasher.smasher.type ) * 360;

                entity.priorityPersistence = false;

                if ( timer > 50 )
                {
                    if ( Math.abs ( entity.smasher.position.y - entity.smasherInitialY ) < 350 && timer < 110 )
                    {
                        entity.smasher.position.y = entity.smasherInitialY + Math.sign ( entity.smasher.smasher.type ) * ( timer - 50 ) * 22;

                        entity.priorityPersistence = true;
                    }
                    else if ( !entity.smashed && timer < 100 )
                    {
                        entity.smashed = true;
                        mario.playSmashEffect ( );
                    }
                    else if ( timer > 110 )
                    {
                        entity.priorityPersistence = true;

                        if ( Math.abs ( entity.smasher.position.y - entity.smasherInitialY ) > 6 )
                            entity.smasher.position.y -= Math.sign ( entity.smasher.smasher.type ) * 5;
                        else 
                            entity.smasher.position.y = entity.smasherInitialY;

                        entity.retracted = true;
                        entity.smashed = false;
                    }
                }
                else { entity.smasher.position.y = entity.smasherInitialY; }
            }
        },

        smasher:
        {
            initialize: function ( entity, data )
            {
                entity.renderOrder = -1;
            },

            update: function ( entity, timestamp )
            {
                if ( entity.smasher.type == -1 )
                {
                    entity.bounding = { x: -40, y: -64, w: 80, h: 450 };
                }
                else
                {
                    entity.bounding = { x: -40, y: -448, w: 80, h: 450 };
                }

                mario.behaviourAggressive ( entity, timestamp );
            }
        }
    },

    tileAnimations:
    [
        [ 2, 141, 142, 141 ],   // question block
        [ 66, 135, 138 ],       // grass left
        [ 67, 136, 139 ],       // grass middle
        [ 68, 137, 140 ],       // grass right
        [ 127, 143, 144, 145 ]  // lava
    ],

    // fps limits
    previousTimestamp: 0,
    fpsLimit: 36,

    maxVelocity: 20,

    world: undefined,
    level: undefined,
    levelName: undefined,
    canvas: undefined,
    context: undefined,
    tileset: undefined,
    progression: undefined,

    jumpProperties:
    {
        jumpHeight: 16.25,
        jumpBoost: 1.85,
        jumpBoostTimer: 4
    },

    powerUp:
    {
        none: 0,
        mushroom: 1,
        fireflower: 2
    },

    gamemode:
    {
        score: 0,
        coins: 0,
        lives: 5,
        powerup: 0,
        time: 0
    },

    logo: undefined,

    entities: [ ],
    backgrounds: [ ],
    sounds: { },
    soundInstances: [ ],

    animations: { },
    templates: { },

    isLevelLoaded: false,
    isGameOver: false,
    isPaused: false,
    isFullyPaused: false,
    onlyEngine: false,
    displayHitboxes: false,

    gameOverTimer: 0,
    levelLoadTimeout: 0,

    keyPressed:
    {
        up: false,
        down: false,
        left: false,
        right: false,
        sprint: false,
        pause: false,
        space: false,
        debug: false
    },

    tilesetWidth: 0,
    tilesetHeight: 0,
    tilesetDimX: 0,
    tilesetDimY: 0,

    loadingTarget: 0,
    loadingProgress: 0,

    randomBackgroundNumber: 0,
    menuTimer: 10,
    menuPromptVisible: true,

    player: undefined,
    
    animationTimer: 0,

    currCoinSound: 0,
    currKickSound: 0,

    marioIcon: undefined,
    spritefont: undefined,
    coinIcon: undefined,
    godMode: false,

    brickParticles: [ ],
    brickTexture: undefined,
    rainTexture: undefined,
    rain: false,

    gameStartTimestamp: undefined,

    // used to control the game when in engine mode
    hooks: 
    {
        onLevelFail: undefined,
        onLevelSuccess: undefined
    },

    smashEffectTimer: 0,

    backgroundOffset: { x: 0, y: 0 },

    // damage sources for enemies
    playerFireballs: [ ],
    shells: [ ],
    bumpDamage: [ ],

    // camera controls
    cameraLock: -1,

    // since the camera will only follow in one direction
    // we only use x coordinate here
    camera:
    {
        x: 0
    },

    fpsMeter:
    {
        lastDelta: 0,
        frames: 0,
        fps: 0
    },

    comboParticles: [ ],
    comboParticlesTileset: undefined,
    comboParticleLifetimeMax: 30,
    comboValues: [ 100, 200, 400, 800, 1000, 2000, 4000, 8000 ],
    currentCombo: 0,

    giveScoreCombo: function ( x = undefined, y = undefined )
    {
        mario.giveScore ( mario.currentCombo, x, y );
        mario.currentCombo++;
    },

    giveScore: function ( value = 0, x = undefined, y = undefined )
    {
        if ( !x || !y )
        {
            x = mario.player.position.x,
            y = mario.player.position.y
        }

        if ( value > mario.comboValues.length - 1 ) value = mario.comboValues.length - 1;
        else if ( value < 0 ) value = 0;

        var numberValue = mario.comboValues [value];
        
        mario.createComboParticle ( x, y, value );
        mario.gamemode.score += numberValue;
    },

    setCameraLock: function ( lock )
    {
        if ( lock < mario.cameraLock || mario.cameraLock < 640 )
            mario.cameraLock = lock;
    },

    calculateWarpPosition: function ( warp )
    {
        var direction = warp.warpEntity.warp.direction;

        if ( direction == 0 ) return { x: warp.position.x + 32, y: warp.position.y + 64 };
        else if ( direction == 1 ) return { x: warp.position.x + 32, y: warp.position.y - 64 };
        else if ( direction == 2 ) return { x: warp.position.x + 64, y: warp.position.y + 32 };
        else return { x: warp.position.x - 32, y: warp.position.y + 32 };
    },

    playSmashEffect: function ( )
    {
        if ( mario.smashEffectTimer <= 0 )
        {
            mario.playSound ( 'snd_smasher' );
            mario.smashEffectTimer = 6;
        }
    },

    warpPlayer: function ( destination )
    {
        var warp = mario.world.warpIndex [destination];
        if ( !warp ) return;

        for ( var i = 0; i < mario.entities.length; ++i )
        {
            if ( mario.entities [i] == mario.player )
            {
                mario.entities.splice ( i, 1 );
                mario.setGameWorld ( warp.level );
                mario.level.entities.push ( mario.player );

                mario.player.position = mario.calculateWarpPosition ( warp );
            }
        }
    },

    destroyAllEnemies: function ( )
    {
        for ( var i = 0; i < mario.entities.length; ++i )
        {
            var entity = mario.entities [i];

            if ( entity.isEnemy )
            {
                mario.spawnEntity ( entity.position.x, entity.position.y, 63 );
                entity.dead = true;
            }
        }
    },

    destroyAllProjectiles: function ( )
    {
        for ( var i = 0; i < mario.entities.length; ++i )
        {
            var entity = mario.entities [i];

            if ( [ 'spiny_egg', 'fireball', 'hammer' ].indexOf ( entity.type ) != -1 )
                entity.dead = true;
        }
    },

    behaviourKnockout: function ( entity, timestamp )
    {
        if ( !entity.knockTimer ) entity.knockTimer = 0;
        if ( entity.physics ) entity.physics.disabled = true;

        entity.knockTimer++;

        entity.position.x += 6;

        if ( entity.knockTimer < 12 ) entity.position.y -= 20 / entity.knockTimer;
        else entity.position.y += 13 * ( entity.knockTimer / 15 );

        if ( entity.position.y > 500 ) entity.dead = true;
    },

    getEntityBounding: function ( entity )
    {
        if ( !entity.bounding )
            entity.bounding = { x: -entity.origin.x, y: -entity.origin.y, w: entity.dimensions.x, h: entity.dimensions.y };

        return bounding = entity.bounding;
    },

    behaviourDamagableEnemy: function ( entity, timestamp )
    {
        var bounding = mario.getEntityBounding ( entity );

        if ( !entity.ignoreShells )
        {
            for ( var i = 0; i < mario.shells.length; ++i )
            {
                var shell = mario.shells [i];
                var shellBounding = mario.getEntityBounding ( shell );

                if ( mario.intersects ( bounding.x + entity.position.x, bounding.y + entity.position.y, bounding.w, bounding.h, shellBounding.x + shell.position.x, shellBounding.y + shell.position.y, shellBounding.w, shellBounding.h ) )
                {
                    if ( Math.abs ( shell.physics.velocity.x ) < 1 ) continue;
                    if ( typeof entity.onShellHit === 'function' ) entity.onShellHit ( shell );
                }
            }
        }

        if ( !entity.ignoreFireballs )
        {
            for ( var i = 0; i < mario.playerFireballs.length; ++i )
            {
                var fireball = mario.playerFireballs [i];
                var fireballBounding = mario.getEntityBounding ( fireball );

                if ( fireball.hasOwnProperty ( 'deathTimer' ) ) continue;

                if ( mario.intersects ( entity.position.x - entity.origin.x, entity.position.y - entity.origin.y, entity.dimensions.x, entity.dimensions.y, fireballBounding.x + fireball.position.x, fireballBounding.y + fireball.position.y, fireballBounding.w, fireballBounding.h ) )
                {
                    fireball.deathTimer = 6;
                    if ( typeof entity.onFireballHit === 'function' ) entity.onFireballHit ( fireball );
                }
            }
        }

        if ( !entity.ignoreBump )
        {
            var feetIndex = mario.pointToIndex ( Math.floor ( entity.position.x / 32 ), Math.floor ( entity.position.y / 32 ) + 1 );

            for ( var i = 0; i < mario.bumpDamage.length; ++i )
            {
                if ( mario.bumpDamage [i].index == feetIndex )
                {
                    mario.bumpDamage.splice ( i, 1 );
                    if ( typeof entity.onBlockHit === 'function' ) entity.onBlockHit ( );
                }
            }
        }

        return true;
    },

    behaviourAggressive: function ( entity, timestamp )
    {
        if ( !mario.player ) return;
        var player = mario.player;

        var bounding = mario.getEntityBounding ( entity );

        var playerRect = { x: player.position.x - player.origin.x, y: player.position.y - player.origin.y, w: player.dimensions.x, h: player.dimensions.y };

        if ( mario.player.crouching && mario.player.powerUp > 0 )
            playerRect = { x: player.position.x - player.origin.x, y: player.position.y - 16, w: 32, h: 32 };

        if ( mario.intersects ( bounding.x + entity.position.x, bounding.y + entity.position.y, bounding.w, bounding.h, playerRect.x, playerRect.y, playerRect.w, playerRect.h ) )
        {
            if ( !entity.damage ) entity.damage = 1;
            mario.playerHit ( entity.damage );
        }
    },

    behaviourStompable: function ( entity, timestamp )
    {
        if ( !mario.player ) return false;
        var player = mario.player;

        if ( player.invulnerability ) return;

        var bounding = mario.getEntityBounding ( entity );

        var playerRect = { x: player.position.x - player.origin.x, y: player.position.y - player.origin.y, w: player.dimensions.x, h: player.dimensions.y };

        if ( mario.intersects ( bounding.x + entity.position.x, bounding.y + entity.position.y - 15, bounding.w, bounding.h + 15, playerRect.x, playerRect.y, playerRect.w, playerRect.h ) )
        {
            if ( mario.player.position.y < entity.position.y + entity.dimensions.y && mario.player.physics.velocity.y > 0 )
            {
                mario.player.physics.velocity.y = -18;
                mario.player.jumpBoostTimeout = 3;
                mario.player.position.y = entity.position.y - entity.origin.y;
                mario.player.invulnerability = 4;
                mario.player.isJumping = true;
                mario.player.stomps++;

                if ( typeof entity.onStomp == 'function' ) entity.onStomp ( );

                return true;
            }
        }

        return false;
    },

    behaviourCareful: function ( entity, timestamp )
    {
        var tileX = Math.floor ( ( entity.position.x - 16 ) / 32 );
        var tileY = Math.floor ( entity.position.y / 32 ) + 1;

        var currentIndex = mario.pointToIndex ( tileX, tileY );

        tileX += Math.sign ( entity.physics.velocity.x );
        if ( entity.physics.velocity.x < 0 ) tileX++;
        
        var index = mario.pointToIndex ( tileX, tileY );

        if ( mario.level.tiles [index] )
        {
            var tile = mario.level.tiles [index][0];
            var collisionMask = 0;

            if ( tile > 0 ) collisionMask = tilesetMask [tile - 1];

            if ( collisionMask != 1 && collisionMask != 2 )
            {
                entity.physics.velocity.x = -entity.physics.velocity.x;
            }
        }

        mario.behaviourSimple ( entity, timestamp );
    },

    behaviourSimple: function ( entity, timestamp )
    {
        if ( !entity.initialized )
        {
            if ( !mario.player ) return;

            var direction = -Math.sign ( entity.position.x - mario.player.position.x );
            if ( direction == 0 ) direction = -1;

            entity.direction = direction;
            entity.physics.velocity.x = entity.direction * 3;

            entity.initialized = true;
        }

        entity.physics.bounceX = true;

        var newDirection = entity.direction = Math.sign ( entity.physics.velocity.x );
        if ( newDirection != 0 ) entity.direction = newDirection;
    },

    mushroomUpdate: function ( entity, data )
    {
        var sgn = 1;
        if ( data.attract ) sgn = -1;

        if ( Math.abs ( entity.physics.velocity.x ) == 0 )
        {
            entity.physics.bounceX = true;
            entity.physics.velocity.x = -3 * sgn;

            if ( player )
            {
                if ( player.position.x < entity.position.x ) entity.velocity.x = 3 * sgn;
            }
        }

        var player = mario.player;
        if ( !player ) return;

        if ( mario.intersects ( entity.position.x, entity.position.y, 32, 32, player.position.x, player.position.y, player.dimensions.x, player.dimensions.y ) )
        {
            entity.runFunction ( );
            entity.dead = true;
        }
    },

    playerHit: function ( damage )
    {
        if ( !mario.player ) return;
        if ( mario.player.invulnerability > 0 && damage < 999 ) return;

        if ( damage >= 999 )
        {
            mario.player.powerUp = -999;
            mario.player.physics.velocity = { x: 0, y: 0 };
            mario.gamemode.powerup = 0;
            return;
        }

        if ( mario.player.powerUp > 0 ) mario.playSound ( 'snd_hit' );
        else mario.player.physics.velocity = { x: 0, y: 0 };

        if ( !damage ) damage = 1;
        if ( !mario.godMode ) mario.player.powerUp -= damage;

        if ( mario.player.powerUp >= 0 ) 
        {
            mario.player.invulnerability = 35;
            mario.player.blinkingTimer = 35;
        }

        console.log ( 'invulnerability: ' + mario.player.invulnerability );

        mario.gamemode.powerup = mario.player.powerUp;
    },

    playerPowerup: function ( powerup )
    {
        if ( !mario.player ) return;

        mario.playSound ( 'snd_buff' );
        // mario.gamemode.score += 1000;
        mario.giveScore ( 4, mario.player.position.x, mario.player.position.y );

        if ( mario.player.powerUp < powerup )
        {
            mario.player.powerUp = powerup;
        }

        mario.gamemode.powerup = mario.player.powerUp;
    },

    playCoinSound: function ( ) // there were just too many... 
    {
        mario.playSound ( 'snd_coin_' + mario.currCoinSound );
        mario.currCoinSound++;

        if ( mario.currCoinSound > 14 ) mario.currCoinSound = 0;
    },

    playKickSound: function ( )
    {
        mario.playSound ( 'snd_kick_' + mario.currKickSound );
        mario.currKickSound++;

        if ( mario.currKickSound > 9 ) mario.currKickSound = 0;
    },

    stopSound: function ( name, reset )
    {
        if ( !mario.sounds [name] ) return;

        var sound = mario.sounds [name];
        sound.audio.pause ( );
        
        if ( reset ) sound.audio.currentTime = 0;
    },

    playSound: function ( name )
    {
        if ( !mario.sounds [name] ) return;

        var sound = mario.sounds [name];

        if ( sound.loop >= 0 )
        {
            sound.audio.addEventListener ( 'timeupdate', function ( )
            {
                if ( this.currentTime > this.duration - 0.11 ) // we give the song a little buffer to create seamless loop
                {
                    this.currentTime = sound.loop / 1000;
                    this.play ( );
                }
            }, false );
        }

        if ( !sound.audio.paused )
        {
            sound.audio.pause ( );
            sound.audio.currentTime = 0;
        }

        sound.audio.play ( );
    },

    renderTile: function ( x, y, index )
    {
        if ( !mario.context )
        {
            throw new Error ( 'Cannot render tile: Context not initialized!' );
        }

        index = index - 1;
        if ( index < 0 || index > mario.tilesetDimX * mario.tilesetDimY ) return;

        var tileX = ( index % mario.tilesetDimX ) * 32;
        var tileY = ( Math.floor ( index / mario.tilesetDimX ) ) * 32;

        mario.context.drawImage ( mario.tileset, tileX, tileY, 32, 32, x, y, 32, 32 );
    },

    renderBackground: function ( currentBackground )
    {
        if ( mario.backgrounds.length >= currentBackground + 1 )
        {
            var backgroundWidth = mario.backgrounds [currentBackground].width;
            var backgroundX = Math.floor ( mario.camera.x / 5 ) + mario.backgroundOffset.x;
            
            backgroundX = backgroundX % backgroundWidth;
            backgroundX = -backgroundX;

            mario.context.drawImage ( mario.backgrounds [currentBackground], Math.floor ( backgroundX ), Math.floor ( mario.backgroundOffset.y ) );

            if ( backgroundX + backgroundWidth < 640 )
                mario.context.drawImage ( mario.backgrounds [currentBackground], Math.floor ( backgroundX + backgroundWidth ), Math.floor ( mario.backgroundOffset.y ) );

            if ( backgroundX > 0 )
                mario.context.drawImage ( mario.backgrounds [currentBackground], Math.floor ( backgroundX - backgroundWidth ), Math.floor ( mario.backgroundOffset.y ) );
        }
    },

    renderEntity: function ( entity )
    {
        if ( entity.onRender ) entity.onRender ( );
        if ( !entity.animation ) return;

        if ( entity.blinkingTimer )
        {
            if ( entity.blinkingTimer > 0 )
            {
                entity.blinkingTimer--;
                if ( Math.floor ( mario.animationTimer / 5 ) % 2 == 0 ) return;
            }
        }

        var x = entity.position.x - entity.origin.x - mario.camera.x;
        var y = entity.position.y - entity.origin.y;

        x = Math.round ( x );
        y = Math.round ( y );

        var ew = entity.dimensions.x;
        var eh = entity.dimensions.y;

        if ( !mario.intersects ( x, y, ew, eh, 0, 0, 640, 480 ) ) return;

        var animationTimer = mario.animationTimer + entity.animationTimer;

        var animation = mario.animations [entity.animation];
        var animationFrames = Math.floor ( animation.width / ew );
        var animationFrame = Math.floor ( animationTimer / mario.fpsLimit * entity.animationSpeed / 1.5 ) % animationFrames;
        
        var sx = animationFrame * ew;
        
        mario.context.drawImage ( animation, sx, 0, ew, eh, x, y, ew, eh );
        if ( mario.displayHitboxes ) mario.renderEntityBounding ( entity );
    },

    renderEntityBounding: function ( entity )
    {
        var bounding = mario.getEntityBounding ( entity );

        var dx = Math.round ( entity.position.x ) + bounding.x - mario.camera.x;
        var dy = Math.round ( entity.position.y ) + bounding.y;

        mario.context.beginPath ( );
        mario.context.rect ( dx, dy, bounding.w, bounding.h );
        mario.context.strokeStyle = 'magenta';
        mario.context.stroke ( );
    },

    pointToIndex: function ( x, y )
    {
        if ( x < 0 || y < 0 || x > mario.level.size || y > 15 )
            return -1;

        return ( mario.level.size * y + x );
    },

    intersects: function ( x, y, w, h, dx, dy, dw, dh )
    {
        if ( x <= ( dw + dx ) && ( x + w ) >= dx && y <= ( dy + dh ) && ( y + h ) >= dy )
        {
            return true;
        }

        return false;
    },

    intersectsBox: function ( x, y, w, h, dx, dy, dw, dh )
    {
        return ( Math.abs ( x - dx ) * 2 < ( w + dw ) ) &&
            ( Math.abs ( y - dy ) * 2 < ( h + dh ) );
    },

    minkowski: function ( x, y, w, h, dx, dy, dw, dh )
    {
        var m = 
        {
            x: x - dx - dw,
            y: y - dy - dh,
            w: w + dw,
            h: h + dh
        };

        return m;
    },

    reloadLevel: function ( )
    {
        mario.unloadLevel ( );
        mario.loadLevel ( mario.levelName );
        mario.isPaused = false;
    },

    unloadLevel: function ( )
    {
        // cleanup
        mario.entities = [];
        mario.stopLevelMusic ( );
    },

    stopLevelMusic: function ( )
    {
        if ( mario.level.music == 0 ) mario.stopSound ( 'bgm_overworld', true );
        else if ( mario.level.music == 1 ) mario.stopSound ( 'bgm_underground', true );
        else if ( mario.level.music == 2 ) mario.stopSound ( 'bgm_athletic', true );
        else if ( mario.level.music == 3 ) mario.stopSound ( 'bgm_airship', true );
        else if ( mario.level.music == 4 ) mario.stopSound ( 'bgm_castle', true );
        else if ( mario.level.music == 5 ) mario.stopSound ( 'bgm_ghost', true );
        else if ( mario.level.music == 6 ) mario.stopSound ( 'bgm_bowser', true );
        else if ( mario.level.music == 7 ) mario.stopSound ( 'bgm_fortress', true );


        mario.stopSound ( 'bgm_bonus' );
    },

    playLevelMusic: function ( )
    {
        if ( mario.level.music == 0 ) mario.playSound ( 'bgm_overworld' );
        else if ( mario.level.music == 1 ) mario.playSound ( 'bgm_underground' );
        else if ( mario.level.music == 2 ) mario.playSound ( 'bgm_athletic' );
        else if ( mario.level.music == 3 ) mario.playSound ( 'bgm_airship' );
        else if ( mario.level.music == 4 ) mario.playSound ( 'bgm_castle' );
        else if ( mario.level.music == 5 ) mario.playSound ( 'bgm_ghost' );
        else if ( mario.level.music == 6 ) mario.playSound ( 'bgm_bowser' );
        else if ( mario.level.music == 7 ) mario.playSound ( 'bgm_fortress' );

        mario.stopSound ( 'bgm_bonus' );
    },

    loadLevel: function ( levelName )
    {
        mario.levelName = levelName;

        var xhr = new XMLHttpRequest ( );
        mario.loadingTarget++;

        xhr.addEventListener ( 'readystatechange', function ( event )
        {
            if ( this.readyState == 4 )
            {
                if ( this.status == 200 )
                {
                    mario.loadLevelRaw ( this.responseText );
                }
                else
                {
                    throw new Error ( 'Failed to load level: ' + levelName + '!' );
                }
            }
        } );

        xhr.open ( 'GET', 'levels/' + levelName + '.json' );
        xhr.send ( );
    },

    loadLevelRaw: function ( data )
    {
        try
        {
            mario.world = JSON.parse ( data );

            console.log ( 'Level loaded successfully' );
            mario.initializeLevel ( );

            mario.isLevelLoaded = true;
        }
        catch ( e )
        {
            console.error ( e );
            throw new Error ( 'Failed to load level' );
        }
    },

    initializeLevel: function ( )
    {
        mario.backgroundOffset = { x: 0, y: 0 };

        mario.player = undefined;
        mario.camera.x = 0;
        mario.loadingProgress++;
        mario.levelLoadTimeout = 60;

        mario.world.overworld.entities = [ ];
        mario.world.bonus.entities = [ ];

        // initialize data for bonus
        mario.setGameWorld ( 1 );
        mario.world.warpIndex = { };

        for ( var i = 0; i < mario.world.warps.length; ++i )
            mario.world.warpIndex [mario.world.warps [i].name] = mario.world.warps [i];

        for ( var i = 0; i < mario.world.warps.length; ++i )
        {
            var warp = mario.world.warps [i];

            if ( warp.level == 1 ) 
            {
                var warpEntity = mario.spawnEntity ( warp.position.x + 16, warp.position.y + 16, warp.id );
                warpEntity.warpData = warp;
                warp.warpEntity = warpEntity;
            }
        }

        mario.preloadEntities ( );

        // initialize data for overworld
        mario.setGameWorld ( 0 );

        for ( var i = 0; i < mario.world.warps.length; ++i )
        {
            var warp = mario.world.warps [i];

            if ( warp.level == 0 ) 
            {
                var warpEntity = mario.spawnEntity ( warp.position.x + 16, warp.position.y + 16, warp.id );
                warpEntity.warpData = warp;
                warp.warpEntity = warpEntity;
            }
        }

        mario.preloadEntities ( );

        if ( !mario.onlyEngine )
        {
            var currentProgress = mario.progression.currentLevel;
            mario.gamemode.time = mario.progression.progression [currentProgress].time;
        }
        // if ( !mario.levelMusicActive ) mario.playLevelMusic ( );
    },

    preloadEntities: function ( )
    {
        // preload for priority entities
        for ( var i = 0; i < mario.level.tiles.length; ++i )
        {
            var tile = mario.level.tiles [i];

            var wx = Math.floor ( i % mario.level.size ) * 32;
            var wy = Math.floor ( i / mario.level.size ) * 32;

            if ( tile [0] == -1 && [ 55, 57, 59, 64, 65, 66 ].indexOf ( tile [1] ) != -1 )
            {
                mario.spawnEntity ( wx + 16, wy + 16, tile [1] );
                mario.level.tiles [i] = [0, 0];
            }
        }
    },

    setGameWorld: function ( id )
    {
        if ( id == 0 && mario.level == mario.world.overworld ) return;
        if ( id == 1 && mario.level == mario.world.bonus ) return;

        mario.backgroundOffset = { x: 0, y: 0 };

        if ( mario.level && mario.world.overworld.music != mario.world.bonus.music ) 
            mario.stopLevelMusic ( );

        if ( id == 0 ) mario.level = mario.world.overworld;
        else mario.level = mario.world.bonus;

        mario.entities = mario.level.entities;
        
        if ( mario.world.overworld.music != mario.world.bonus.music ) mario.playLevelMusic ( );
    },

    setCookie: function ( name, data, days )
	{
		name = name.replace ( /\s/g, '_' );
		
		var date = new Date ( );
		date.setTime ( date.getTime ( ) + ( days * 24 * 60 * 60 * 1000 ) );
		
		document.cookie = name + '=' + data + '; expires=' + date.toUTCString ( ) + '; path=/';
	},
	
	getCookie: function ( name )
	{
		var cookie_array = document.cookie.split ( ';' );
		
		for ( var i = 0; i < cookie_array.length; ++i )
		{
			var key_value = cookie_array [i].split ( '=' );
			key_value [0] = key_value [0].replace ( /\s/g, '' );
			
			if ( key_value [0] == name ) return key_value [1];
		}
		
		return undefined;
	},
	
	removeCookie: function ( name )
	{
		document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/'; 
    },

    serializeSaveData: function ( )
    {
        var saveData =
        {
            currentLevel: mario.progression.currentLevel,
            currentScore: mario.gamemode.score,
            currentCoins: mario.gamemode.coins,
            currentPowerup: mario.gamemode.powerup,
            currentLives: mario.gamemode.lives
        };

        return JSON.stringify ( saveData );
    },

    parseSaveData: function ( raw )
    {
        try
        {
            var saveData = JSON.parse ( raw );

            mario.progression.currentLevel = saveData.currentLevel + 1;
            mario.progression.currentProgress = saveData.currentLevel;
            mario.gamemode.score = saveData.currentScore;
            mario.gamemode.coins = saveData.currentCoins;
            mario.gamemode.lives = saveData.currentLives;
            mario.gamemode.powerup = saveData.currentPowerup;
        } 
        catch ( e )
        {
            // console.warn ( 'invalid savedata format' );
            mario.progression.currentLevel = 0;
            mario.progression.currentProgress = 0;
            mario.gamemode.score = 0;
            mario.gamemode.coins = 0;
            mario.gamemode.lives = 5;
            mario.gamemode.powerup = 0;
        }
    },

    progressionLoad: function ( )
    {
        mario.parseSaveData ( mario.getCookie ( 'gameProgress' ) );

        mario.gameStartTimestamp = new Date ( );
        mario.loadLevel ( mario.progression.progression [mario.progression.currentLevel].levelName );
    },

    progressionFail: function ( )
    {
        if ( mario.onlyEngine )
        {
            mario.isLevelLoaded = false;
            mario.unloadLevel ( );

            mario.level = { };
            mario.world = { };

            if ( mario.hooks.onLevelFail ) mario.hooks.onLevelFail ( );
            return;
        }

        if ( mario.gamemode.lives > 0 )
        {
            mario.reloadLevel ( );
        }
        else
        {
            mario.backgroundOffset = { x: 0, y: 0 };

            mario.level = { };
            mario.entities = { };
            mario.player = undefined;

            console.log ( 'the player has lost all the lives' );
            console.log ( 'displaying gameover screen' );

            mario.gameOverTimer = 300;
            mario.playSound ( 'bgm_gameover' );
        }
    },

    progressionSuccess: function ( )
    {
        if ( mario.onlyEngine )
        {
            mario.isLevelLoaded = false;
            mario.unloadLevel ( );

            mario.level = { };
            mario.world = { };

            if ( mario.hooks.onLevelFail ) mario.hooks.onLevelFail ( );
            return;
        }

        var currentLevel = mario.progression.progression [mario.progression.currentLevel];

        // remove stuff to prevent overwriting data
        mario.level = { };
        mario.entities = { };
        mario.player = undefined;

        // if level was marked as checkpoint (castle)
        // then we save the data here
        if ( currentLevel.levelType == 1 )
        {
            mario.progression.currentProgress = mario.progression.currentLevel;
            mario.setCookie ( 'gameProgress', mario.serializeSaveData ( ), 999 );
        }

        mario.progression.currentLevel++;

        if ( mario.progression.progression.length <= mario.progression.currentLevel )
        {
            mario.backgroundOffset = { x: 0, y: 0 };

            mario.level = { };
            mario.entities = { };
            mario.player = undefined;

            // reset the gamemode;
            mario.gamemode = 
            {
                score: 0,
                coins: 0,
                lives: 5,
                powerup: 0
            };

            mario.isLevelLoaded = false;
            mario.isPaused = false;

            mario.resetAllSounds ( );
        }

        mario.gameStartTimestamp = new Date ( );
        console.log ('progression success, loading level ' + mario.progression.currentLevel + ' with name ' + mario.progression.progression [mario.progression.currentLevel].levelName);
        mario.loadLevel ( mario.progression.progression [mario.progression.currentLevel].levelName );
    },

    spawnEntity: function ( x, y, id )
    {
        console.log ( 'spawning ' + id + ' at ' + x + ', ' + y );

        // if entity is null then fall through
        if ( mario.templates.length <= id ) return;
        if ( !mario.templates [id] ) return;

        var template = mario.templates [id];

        var isEnemy = false;
        if ( template.enemy ) isEnemy = true;

        var entity =
        {
            type: template.name,
            position: { x: x, y: y },
            persistent: template.persistent,
            dimensions:
            {
                x: template.dimensions [0],
                y: template.dimensions [1]
            },
            origin:
            {
                x: template.origin [0],
                y: template.origin [1]
            },
            animation: template.anim,
            animationSpeed: 10,
            animationTimer: 0,
            isEnemy: isEnemy,
            components: [ ]
        };

        for ( var i = 0; i < template.components.length; ++i )
        {
            var component = JSON.parse ( JSON.stringify ( template.components [i] ) ); // copy
            
            if ( mario.components [component.name] )
            {
                entity [component.name] = component;
                entity.components.push ( component.name );
                mario.components [component.name].initialize ( entity );
            }
        }

        mario.entities.push ( entity );
        return entity;
    },

    renderMenu: function ( )
    {
        mario.renderBackground ( mario.randomBackgroundNumber );

        var logoWidth = mario.logo.width;
        
        var logoX = ( 640 - logoWidth ) / 2;
        var logoY = 70;

        mario.context.drawImage ( mario.logo, logoX, logoY );

        if ( mario.randomBackgroundNumber == 0 ) mario.context.fillStyle = '#000000';
        else mario.context.fillStyle = '#ffffff';

        if ( mario.menuPromptVisible )
        {
            mario.context.font = '16px nes';
            mario.context.fillText ( 'Press Space to Start!', 160, 350 );
        }

        mario.context.font = '14px nes';
        mario.context.fillText ( 'created by eter', 400, 460 );
        mario.context.fillText ( 'dev 0.5', 15, 460 );
    },

    tileAnimate: function ( index )
    {
        var tile = mario.level.tiles [index][0];

        if ( tilesetAnimations.length < tile ) return;
        var animationID = tilesetAnimations [ tile - 1 ];

        if ( animationID <= 0 ) return;

        var animation = mario.tileAnimations [ animationID - 1 ];
        var totalFrames = animation.length;
        var currentFrame = Math.floor ( mario.animationTimer / 6 ) % totalFrames;

        mario.level.tiles [index][0] = animation [currentFrame];
    },

    renderSpriteFontText: function ( x, y, text )
    {
        var spriteFontWidth = mario.spritefont.width / 20;
        var spriteFontHeight = mario.spritefont.height / 20;

        var nx = 0;
        for ( var i = 0; i < text.length; ++i )
        {
            var ch = text.charAt ( i );

            if ( ch === ' ' ) 
            {
                nx += 8;
                continue;
            }

            if ( spritefontData.hasOwnProperty ( ch ) )
            {
                var id = spritefontData [ch];
                var sx = id % spriteFontWidth;
                var sy = Math.floor ( id / spriteFontWidth );

                mario.context.drawImage ( mario.spritefont, sx * 20, sy * 20, 20, 20, x + nx, y, 17, 17 );
            }

            nx += 17;
        }
    },

    renderHUD: function ( )
    {
        mario.renderSpriteFontText ( 10, 10, 'mario x ' + mario.gamemode.lives );
        mario.renderSpriteFontText ( 10, 30, ( '0000000' + mario.gamemode.score ).slice ( -7 ) );

        mario.context.drawImage ( mario.coinIcon, 210, 15, 25, 25 );
        mario.renderSpriteFontText ( 230, 19, ' x ' + ( '00' + mario.gamemode.coins ).slice ( -2 ) );

        if ( !mario.onlyEngine )
        {
            mario.renderSpriteFontText ( 370, 10, 'world' );
            mario.renderSpriteFontText ( 385, 30, mario.progression.progression [mario.progression.currentLevel].displayName );
        }

        mario.renderSpriteFontText ( 550, 10, 'time' );
        mario.renderSpriteFontText ( 568, 30, ( '00' + mario.gamemode.time ).slice ( -3 ) );
    },

    renderRain: function ( )
    {
        if ( !mario.rain ) return;

        var rainOffset = 
        {
            x: ( mario.animationTimer * -2.5 - mario.camera.x / 2 ) % 640,
            y: ( mario.animationTimer * 13.5 ) % 480
        }

        mario.context.drawImage ( mario.rainTexture, rainOffset.x, rainOffset.y );
        mario.context.drawImage ( mario.rainTexture, rainOffset.x + 640, rainOffset.y );
        mario.context.drawImage ( mario.rainTexture, rainOffset.x, rainOffset.y - 480 );
        mario.context.drawImage ( mario.rainTexture, rainOffset.x + 640, rainOffset.y - 480 );
    },

    renderComboParticles: function ( )
    {
        for ( var i = 0; i < this.comboParticles.length; ++i )
        {
            var particle = this.comboParticles [i];
            var lifetimeMax = mario.comboParticleLifetimeMax;
            var posY = particle.y - Math.abs ( Math.floor ( ( lifetimeMax - particle.lifetime ) * 0.75 ) ) - 9;
            var posX = particle.x - mario.camera.x - 18;

            if ( particle.lifetime <= 0 ) particle.dead = true;
            else particle.lifetime--;

            // rendering code goes here
            mario.context.drawImage ( mario.comboParticlesTileset, 0, 18 * ( particle.index % 8 ), 36, 18, posX, posY, 36, 18 );
        }

        for ( var i = 0; i < this.comboParticles.length; ++i )
        {
            if ( this.comboParticles [i].dead )
                this.comboParticles.splice ( i, 1 );
        }
    },

    renderLevel: function ( )
    {
        if ( mario.levelLoadTimeout > 0 )
        {
            mario.context.drawImage ( mario.marioIcon, 274, 248, 28, 28 );
            
            if ( !mario.onlyEngine )
            {
                mario.renderSpriteFontText ( 243, 210, 'world ' + mario.progression.progression [mario.progression.currentLevel].displayName );
            }

            mario.renderSpriteFontText ( 308, 255, 'x ' + mario.gamemode.lives );
            mario.renderHUD ( );
            return;
        }

        if ( mario.gameOverTimer > 0 )
        {
            mario.renderSpriteFontText ( 243, 210, 'game over' );
            return;
        }

        var currentBackground = mario.level.background;
        mario.renderBackground ( currentBackground );

        var foregroundLayer = [];
        var priorityLayer = [];

        for ( var i = 0; i < mario.entities.length; ++i )
        {
            var entity = mario.entities [i];
            
            if ( entity.renderOrder )
            {
                if ( entity.renderOrder == -1 ) mario.renderEntity ( entity );
                else if ( entity.renderOrder == 0 ) foregroundLayer.push ( entity );
                else if ( entity.renderOrder == 1 ) priorityLayer.push ( entity );
            }
            else { foregroundLayer.push ( entity ); }
        }

        var startTile = Math.floor ( mario.camera.x / 32 );

        for ( var y = 0; y < 15; ++y )
        {
            for ( var x = startTile; x < startTile + 21; ++x )
            {
                if ( x >= mario.level.size ) continue;
                var index = mario.pointToIndex ( x, y );
                    
                var tx = x * 32;
                var ty = y * 32;

                var tile = mario.level.tiles [index][0];

                if ( tile > 0 && mario.animationTimer % 6 === 0 ) mario.tileAnimate ( index );

                if ( tile == -1 )
                {
                    // if rendered tile is -1 then spawn entity and set it to air

                    var objectID = mario.level.tiles [index][1];
                    mario.spawnEntity ( tx + 16, ty + 16, objectID );

                    mario.level.tiles [index] = [0, 0];
                    continue;
                }

                var py = 0;

                if ( mario.level.tiles [index][2] )
                {
                    if ( mario.level.tiles [index][2] >= 6 ) mario.level.tiles [index].splice ( 2, 1 );
                    else
                    {
                        py = Math.floor ( ( Math.abs ( 4 * mario.level.tiles [index][2] - 12 ) - 12 ) * 0.75 ); // min = -9
                        mario.level.tiles [index][2] += 1;
                    }
                }

                tx -= mario.camera.x;
                mario.renderTile ( tx, ty + py, tile );
            }
        }

        for ( var i = 0; i < foregroundLayer.length; ++i ) mario.renderEntity ( foregroundLayer [i] );
        for ( var i = 0; i < priorityLayer.length; ++i ) mario.renderEntity ( priorityLayer [i] );

        mario.updateBlockParticles ( );
        mario.renderRain ( );
        mario.renderHUD ( );
        mario.renderComboParticles ( );
    },

    render: function ( )
    {
        // clear the screen before rendering
        // mario.context.clearRect ( 0, 0, mario.canvas.width, mario.canvas.height );

        // this fixes the rendering bug
        mario.context.fillStyle = 'black';
        mario.context.fillRect ( 0, 0, mario.canvas.width, mario.canvas.height );

        if ( mario.isLevelLoaded )
        {
            mario.renderLevel ( );
        }
        else
        {
            if ( !mario.onlyEngine ) mario.renderMenu ( );
        }

        if ( mario.displayHitboxes )
        {
            mario.renderSpriteFontText ( 20, 450, 'fps ' + mario.fpsMeter.fps );
        }
    },

    createComboParticle: function ( x, y, index )
    {
        var comboParticle = 
        {
            x: x,
            y: y,
            index: index,
            lifetime: mario.comboParticleLifetimeMax
        };

        this.comboParticles.push ( comboParticle );
    },

    updateBlockParticles: function ( )
    {
        for ( var i = 0; i < mario.brickParticles.length; ++i )
        {
            var particle = mario.brickParticles [i];

            if ( particle.vy < 20 ) particle.vy += 1.5;

            particle.x += particle.vx;
            particle.y += particle.vy;

            if ( particle.y > 480 )
            {
                mario.brickParticles.splice ( i, 1 );
                continue;
            }

            // render the particle
            var sx = ( particle.id * 16 );
            var sy = 0;

            mario.context.drawImage ( mario.brickTexture, sx, sy, 16, 16, particle.x - mario.camera.x, particle.y, 16, 16 );
        }
    },

    destroyBlock: function ( index, particleID )
    {
        if ( !particleID ) particleID = 0;
        var x = ( index % mario.level.size ) * 32;
        var y = Math.floor ( index / mario.level.size ) * 32;

        mario.brickParticles.push ( { vy: -12, vx: -4, id: particleID, x: x, y: y } );
        mario.brickParticles.push ( { vy: -8, vx: -4, id: particleID, x: x, y: y + 16 } );
        mario.brickParticles.push ( { vy: -12, vx: 4, id: particleID + 3, x: x, y: y } );
        mario.brickParticles.push ( { vy: -8, vx: 4, id: particleID + 3, x: x, y: y + 16 } );

        mario.level.tiles [index] = [0, 0];
    },

    // this function handles situation when mario bumps the block from the bottom
    bumpBlock: function ( index, force )
    {
        var tile = mario.level.tiles [index][0];
        if ( mario.level.tiles [index][2] ) return;

        var tx = ( index % mario.level.size );
        var ty = Math.floor ( index / mario.level.size );
        
        if ( [ 2, 141, 142, 141 ].indexOf ( tile ) != -1 ) // question mark block
        {
            mario.level.tiles [index][2] = 1;
            var itemID = mario.level.tiles [index][1];

            if ( itemID >= 0 && itemID <= 6 )
            {
                if ( itemID == 5 )
                {
                    if ( mario.player.powerUp == 0 ) itemID = 1;
                    else itemID = 4;
                }

                var replacementID = 3;
                if ( mario.level.background == 1 ) replacementID = 129; 
                setTimeout ( function ( ) { mario.level.tiles [index][0] = replacementID; }, 4 );

                var placeholder = mario.spawnEntity ( tx * 32 + 16, ty * 32 + 16, 46 );
                placeholder.item_placeholder.type = itemID;
            }

            mario.bumpDamage.push ( { index: index } );
        }
        else if ( [ 1, 128, 146 ].indexOf ( tile ) != -1 ) // brick block
        {
            mario.level.tiles [index][2] = 1;

            var particleID = 0;
            if ( tile == 128 ) particleID = 2;
            else if ( tile == 146 ) particleID = 1;

            if ( force > 0 )
            {
                mario.playSound ( 'snd_brick' );
                setTimeout ( function ( ) { mario.destroyBlock ( index, particleID ); }, 4 );
            }

            mario.bumpDamage.push ( { index: index } );
        }
    },

    isSolidTile: function ( index )
    {
        var tile = mario.level.tiles [index][0];
        if ( tile <= 0 ) return false;

        var collisionMask = tilesetMask [tile - 1];
        return ( collisionMask == 1 );
    },

    updateMenu: function ( delta )
    {
        if ( !mario.menuMusicStarted )
        {
            mario.menuMusicStarted = true;
            mario.playSound ( 'bgm_bonus' );
        }

        mario.camera.x += 4;

        if ( mario.menuTimer > 0 ) mario.menuTimer--;

        if ( mario.keyPressed.space ) 
        {
            if ( mario.keyPressed.debug )
            {
                window.location.href = 'editor.html';
            }

            mario.menuMusicStarted = false;
            mario.stopSound ( 'bgm_bonus' );
            mario.progressionLoad ( );
        }

        if ( mario.menuTimer <= 0 )
        {
            mario.menuPromptVisible = !mario.menuPromptVisible;
            mario.menuTimer = 20;
        }
    },

    getPenetrationVector: function ( min )
    {
        var v = { x: 0, y: 0 };

        if ( Math.abs ( min.x ) <= Math.abs ( min.x + min.w ) ) v.x = min.x;
        else v.x = min.x + min.w;

        if ( Math.abs ( min.y ) <= Math.abs ( min.y + min.h ) ) v.y = min.y;
        else v.y = min.y + min.h;

        if ( Math.abs ( v.x ) < Math.abs ( v.y ) ) v.y = 0;
        else v.x = 0;

        v.x = -v.x;
        v.y = -v.y;

        return v;
    },

    resetAllSounds: function ( )
    {
        for ( var k in mario.sounds )
            mario.sounds [k].audio.currentTime = 0;
    },

    updateLevel: function ( delta )
    {
        if ( mario.isFullyPaused ) return;

        if ( mario.levelLoadTimeout > 0 )
        {
            mario.stopLevelMusic ( );
            mario.levelLoadTimeout--;

            if ( mario.levelLoadTimeout == 0 ) mario.playLevelMusic ( );
            return;
        }

        if ( mario.gameOverTimer > 0 )
        {
            mario.gameOverTimer--;
            if ( mario.gameOverTimer == 0 )
            {
                // reset the gamemode;
                mario.gamemode = 
                {
                    score: 0,
                    coins: 0,
                    lives: 5,
                    powerup: 0
                };

                mario.isLevelLoaded = false;
                mario.isPaused = false;

                mario.resetAllSounds ( );
            }
        }

        if ( !mario.isPaused ) mario.animationTimer++;

        if ( mario.player )
        {
            if ( mario.gamemode.time > 0 && mario.animationTimer % 45 == 0 && !mario.isPaused && !mario.player.movementDisabled ) 
            {
                mario.gamemode.time--;

                if ( mario.gamemode.time <= 0 )
                {
                    mario.gamemode.time = 0;
                    mario.playerHit ( 10 );
                }
            }
        }

        var physicObjects = [ ];
        var solidObjects = [ ];

        var player = undefined;

        mario.shells = [ ];
        mario.playerFireballs = [ ];

        mario.rain = false;

        // custom background updates
        if ( mario.level && [5, 11, 12, 13].indexOf ( mario.level.background ) != -1 ) 
        {
            mario.backgroundOffset.x -= 1.5;
            mario.backgroundOffset.y = -120 + Math.round ( Math.sin ( mario.animationTimer / 45 ) * 30 ) + 30;

            mario.rain = true;

            var airshipFrames = [5, 11, 12, 11, 12, 13, 5];

            if ( mario.animationTimer % 220 >= 206 )
            {
                var currentAirshipFrame = Math.floor ( ( ( mario.animationTimer % 220 ) - 206 ) / 2 )
                mario.level.background = airshipFrames [ currentAirshipFrame ];

                if ( mario.animationTimer % 220 == 219 ) mario.playSound ( 'snd_thunder' );
            }
        }

        if ( mario.level && [6, 14, 15, 16].indexOf ( mario.level.background ) != -1 )
        {
            var castleFrames = [6, 14, 15, 14, 15, 16, 6];

            if ( mario.animationTimer % 220 >= 206 )
            {
                var currentCastleFrame = Math.floor ( ( ( mario.animationTimer % 220 ) - 206 ) / 2 )
                mario.level.background = castleFrames [ currentCastleFrame ];

                if ( mario.animationTimer % 220 == 219 ) mario.playSound ( 'snd_thunder_alt' );
            }
        }

        for ( var i = 0; i < mario.entities.length; ++i )
        {
            var entity = mario.entities [i];

            if ( !mario.intersects ( entity.position.x, entity.position.y, entity.dimensions.x, entity.dimensions.y,
                mario.camera.x - 200, -400, 1040, 1040 ) )
            {
                if ( !entity.persistent ) entity.dead = true;
                else continue;
            }

            if ( entity.dead )
            {
                mario.entities.splice ( i, 1 );
                continue;
            }

            if ( entity.player_fireball ) mario.playerFireballs.push ( entity );
            if ( entity.shell ) mario.shells.push ( entity );
        }

        for ( var i = 0; i < mario.entities.length; ++i )
        {
            var entity = mario.entities [i];

            if ( !mario.intersects ( entity.position.x, entity.position.y, entity.dimensions.x, entity.dimensions.y,
                mario.camera.x - 200, -400, 1040, 1040 ) && !entity.priorityPersistence ) continue;

            if ( entity.physics && !mario.isPaused )
            {
                if ( !entity.physics.disabled ) physicObjects.push ( entity );
                if ( !entity.physics.movable ) solidObjects.push ( entity );
            }

            for ( var j = 0; j < entity.components.length; ++j )
            {
                if ( k == "player" ) player = entity;

                var k = entity.components [j];
                if ( !mario.components [k] ) continue;

                if ( mario.components [k].update )
                    mario.components [k].update ( entity, delta );
            }
        }

        mario.bumpDamage = [ ];

        // physics code starts here
        // it includes pretty much everything, from velocities to collisions

        // move platforms to the end of physic objects list bc platform bugs and whatever
        physicObjects.sort ( function ( a, b ) 
        { 
            if ( a.physics.localized && !b.physics.localized ) return 1;
            else if ( !a.physics.localized && b.physics.localized ) return -1;

            return 0;
        } );

        // physic objects interactions
        for ( var i = 0; i < physicObjects.length; ++i )
        {
            var object = physicObjects [i];

            if ( object.physics.gravity ) // apply gravity
                object.physics.velocity.y += 1.5;

            // cap the velocities
            if ( object.physics.velocity.x > mario.maxVelocity ) object.physics.velocity.x = mario.maxVelocity;
            if ( object.physics.velocity.y > mario.maxVelocity ) object.physics.velocity.y = mario.maxVelocity;
            if ( object.physics.velocity.x < -mario.maxVelocity ) object.physics.velocity.x = -mario.maxVelocity;
            if ( object.physics.velocity.y < -mario.maxVelocity ) object.physics.velocity.y = -mario.maxVelocity;

            var origin = object.origin;
            var dimensions = object.dimensions;

            if ( object.physics.movable )
            {
                object.physics.system = undefined;

                // apply the velocity
                var position = 
                { 
                    x: object.position.x,
                    y: object.position.y
                };

                var objectVelocity = object.physics.velocity;

                if ( object.physics.system )
                {
                    objectVelocity.x += object.physics.system.physics.velocity.x;
                    objectVelocity.y += object.physics.system.physics.velocity.y;
                }

                // a very simple solution to a stupid problem
                // to handle edge cases we use the magical technology of
                // QUARTER STEPS
                var steps = 4;
                if ( Math.max ( Math.abs ( objectVelocity.x ), Math.abs ( objectVelocity.y ) ) > 10 ) steps = 8;
                if ( Math.max ( Math.abs ( objectVelocity.x ), Math.abs ( objectVelocity.y ) ) > 15 ) steps = 10;

                for ( var j = 0; j < steps; ++j )
                {
                    var solids = [ ];

                    position.x += objectVelocity.x / steps;
                    position.y += objectVelocity.y / steps;

                    // tile collisions
                    var sx = Math.floor ( ( position.x - dimensions.x ) / 32 );
                    var sy = Math.floor ( ( position.y - dimensions.y ) / 32 );

                    var ex = Math.ceil ( ( position.x + dimensions.x ) / 32 );
                    var ey = Math.ceil ( ( position.y + dimensions.y ) / 32 );

                    for ( var y = sy; y < ey; ++y )
                    {
                        for ( var x = sx; x < ex; ++x )
                        {
                            var index = mario.pointToIndex ( x, y );
                            if ( index < 0 ) continue;

                            if ( !mario.level.tiles [index] ) continue;

                            var tile = mario.level.tiles [index][0];
                            if ( tile <= 0 ) continue;
                            if ( tilesetMask.length < tile ) continue;

                            var collisionMask = tilesetMask [ tile - 1 ];
                            if ( collisionMask == 0 ) continue;

                            solids.push ( { x: x * 32, y: y * 32, w: 32, h: 32, type: collisionMask - 1, index: index } );
                        }
                    }

                    for ( var k = 0; k < solidObjects.length; ++k )
                    {
                        var solid = solidObjects [k];

                        solids.push 
                        ( { 
                            x: solid.position.x - solid.origin.x, 
                            y: solid.position.y - solid.origin.y, 
                            w: solid.dimensions.x, h: solid.dimensions.y, 
                            type: solid.physics.collision, localized: solid.physics.localized, entity: solid
                        } );
                    }

                    for ( var k = 0; k < solids.length; ++k )
                    {
                        var solid = solids [k];
                        // var minkowski = mario.minkowski ( position.x - origin.x, position.y - origin.y, dimensions.x, dimensions.y, solid.x, solid.y, solid.w, solid.h );

                        var bounding = mario.getEntityBounding ( object );
                        var minkowski = mario.minkowski ( position.x + bounding.x, position.y + bounding.y, bounding.w, bounding.h, solid.x, solid.y, solid.w, solid.h );

                        if ( mario.intersects ( 0, 0, 0, 0, minkowski.x, minkowski.y, minkowski.w, minkowski.h ) )
                        {
                            var penetration = mario.getPenetrationVector ( minkowski );

                            if ( solid.type == 0 )
                            {
                                // mario breaking blocks
                                if ( solid.index && penetration.y > 0 && object.isPlayer && object.physics.velocity.y < 0 )
                                {
                                    var tileY = Math.floor ( object.position.y / 32 );
                                    if ( mario.player.powerUp > 0 ) tileY -= 1;

                                    var index = mario.pointToIndex ( Math.floor ( object.position.x / 32 ), tileY - 1 );

                                    if ( mario.isSolidTile ( index ) ) mario.bumpBlock ( index, mario.player.powerUp );
                                    else if ( mario.isSolidTile ( index - 1 ) ) mario.bumpBlock ( index - 1, mario.player.powerUp );
                                    else if ( mario.isSolidTile ( index + 1 ) ) mario.bumpBlock ( index + 1, mario.player.powerUp );

                                    mario.player.jumpBoostTimeout = 0;
                                }

                                // shell breaking blocks
                                if ( solid.index && Math.abs ( penetration.x ) > 0 && object.isShell && Math.sign ( penetration.x ) == -Math.sign ( object.physics.velocity.x ) )
                                {
                                    var tileX = Math.floor ( object.position.x / 32 ) + Math.sign ( object.physics.velocity.x ) * 1;
                                    var index = mario.pointToIndex ( tileX, Math.floor ( object.position.y / 32 ) );

                                    if ( mario.isSolidTile ( index ) ) mario.bumpBlock ( index, 4 );
                                }

                                if ( penetration.y > 0 && object.physics.velocity.y < 0 ) object.physics.velocity.y = 0;
                                else if ( penetration.y < 0 && object.physics.velocity.y > 0 ) object.physics.velocity.y = 0;

                                if ( !object.physics.bounceX )
                                {
                                    if ( penetration.x > 0 ) object.physics.velocity.x += 2;
                                    if ( penetration.x < 0 ) object.physics.velocity.x -= 2;
                                }
                                else
                                {
                                    if ( Math.abs ( penetration.x ) > Math.abs ( object.physics.velocity.x / ( steps + 2 ) ) && Math.sign ( penetration.x ) == -Math.sign ( object.physics.velocity.x ) ) 
                                    {
                                        object.physics.velocity.x = -object.physics.velocity.x;
                                    }
                                }

                                position.x += penetration.x;
                                position.y += penetration.y;
                            }
                            else if ( solid.type == 1 )
                            {
                                var feetPos = ( object.position.y + ( object.dimensions.y - object.origin.y ) );

                                if ( object.physics.velocity.y > 0 ) feetPos = feetPos - object.physics.velocity.y;

                                if ( penetration.y < 0 && object.physics.velocity.y > 0 && feetPos <= solid.y ) 
                                {
                                    object.physics.velocity.y = 0;
                                    position.y += penetration.y;
                                }
                            }
                            else if ( solid.type == 2 && penetration.y < -6 )
                            {
                                object.isLava = true;
                            }
                            
                            if ( solid.localized && penetration.y < 0 )
                                object.physics.system = solid.entity;

                            // this makes fireballs bouncier
                            if ( object.isFireball && penetration.y < 0 )
                                object.fireballBounce = true;
                        }
                    }
                }

                if ( object.physics.velocity.y == 0 ) object.onGroundTimer++;
                else object.onGroundTimer = 0;

                if ( object.isPlayer )
                {
                    if ( position.x < 16 ) position.x = 16;
                }

                if ( object.physics.system )
                {
                    position.x += object.physics.system.physics.velocity.x;
                    position.y += object.physics.system.physics.velocity.y;
                }

                object.position = position;
            }
            else
            {
                object.position.x += object.physics.velocity.x;
                object.position.y += object.physics.velocity.y;
            }
        }

        if ( mario.player )
        {
            var camDirection = Math.sign ( Math.round ( mario.player.position.x ) - 320 - mario.camera.x );
            mario.camera.x += 10 * camDirection;

            if ( Math.abs ( mario.camera.x - mario.player.position.x + 320 ) < 20 || Math.abs ( mario.camera.x - mario.player.position.x + 320 ) > 640 )
                mario.camera.x = Math.round ( mario.player.position.x ) - 320;
        }

        if ( mario.cameraLock >= 640 && mario.camera.x + 640 > mario.cameraLock && player.position.x < mario.cameraLock )
            mario.camera.x = mario.cameraLock - 640;

        mario.cameraLock = -1;

        if ( mario.smashEffectTimer > 0 )
        {
            mario.smashEffectTimer--;

            if ( mario.animationTimer % 2 == 0 ) mario.camera.x += 2;
            else mario.camera.x -= 2;
        }

        if ( mario.camera.x < 0 ) mario.camera.x = 0;

        if ( mario.camera.x > ( mario.level.size - 20 ) * 32 ) 
            mario.camera.x = ( mario.level.size - 20 ) * 32;

        // debug
        if ( mario.keyPressed.debug && mario.keyPressed.menu )
        {
            mario.isFullyPaused = true;
            mario.keyPressed.debug = false;
            mario.keyPressed.menu = false;

            mariodebug.displayDebugMenu ( );
        }
    },

    update: function ( timestamp )
    {
        window.requestAnimationFrame ( mario.update ); // request another frame

        var fpsInterval = 1000 / mario.fpsLimit;
        var now = Date.now ( );
        var elapsed = now - mario.previousTimestamp;

        // fps calculation
        var fpsElapsed = now - mario.fpsMeter.lastDelta;
        if ( fpsElapsed >= 1000 )
        {
            mario.fpsMeter.fps = mario.fpsMeter.frames;
            mario.fpsMeter.frames = 0;
            mario.fpsMeter.lastDelta = now - ( elapsed % 1000 );
        }

        if ( elapsed < fpsInterval ) return;

        mario.previousTimestamp = now - ( elapsed % fpsInterval );
        var delta = elapsed;

        mario.fpsMeter.frames++;

        if ( mario.loadingProgress < mario.loadingTarget )
        {
             // clear the screen before rendering
            mario.context.clearRect ( 0, 0, mario.canvas.width, mario.canvas.height );

            mario.context.fillStyle = '#ffffff';
            mario.context.font = '16px nes';

            mario.context.fillText ( 'Loading some resources...', 140, 350 );

            return;
        }

        if ( mario.isLevelLoaded )
        {
            mario.updateLevel ( delta );
        }
        else
        {
            if ( !mario.onlyEngine ) mario.updateMenu ( delta );
        }

        mario.render ( );
        // mario.renderSpriteFontText ( 10, 460, 'fps ' + mario.fpsMeter.fps );
    },

    initializeAnimations: function ( )
    {
        mario.loadingTarget += animationList.length;

        for ( var i = 0; i < animationList.length; ++i )
        {
            var anim = animationList [i];

            mario.animations [anim] = new Image ( );
            mario.animations [anim].src = 'assets/entities/' + anim + '.png';
            mario.animations [anim].addEventListener ( 'load', function ( event ) { mario.loadingProgress++; } );
            mario.animations [anim].addEventListener ( 'error', function ( event ) { console.warn ( anim + ' failed to load!' ); } );
        }
    },

    loadResourceAJAX: function ( resource, onSuccess, onError )
    {
        var xhr = new XMLHttpRequest ( );
        mario.loadingTarget++;

        xhr.addEventListener ( 'readystatechange', function ( event )
        {
            if ( this.readyState == 4 )
            {
                if ( this.status == 200 ) 
                {
                    onSuccess ( this );
                    mario.loadingProgress++;
                }
                else { onError ( this ); }
            }
        } );

        xhr.open ( 'GET', resource );
        xhr.send ( );
    },

    initializeEntities: function ( )
    {
        mario.loadResourceAJAX ( 'entities.json', 
            function ( xhr )
            {
                mario.templates = JSON.parse ( xhr.responseText );
                console.log ( 'Loaded entity schematics' );
            },
        
            function ( xhr ) 
            { 
                throw new Error ( 'Failed to load entity database!' ); 
        } );
    },

    initializeProgression: function ( )
    {
        mario.loadResourceAJAX ( 'progression.json', 
            function ( xhr )
            {
                mario.progression = JSON.parse ( xhr.responseText );
                console.log ( 'Loaded world progression data' );
            },
        
            function ( xhr ) 
            { 
                throw new Error ( 'Failed to load entity database!' ); 
        } );
    },

    initializeSounds: function ( )
    {
        // bgm
        mario.sounds ['bgm_bonus'] = { audio: new Audio ( 'assets/sound/bgm_bonus.wav' ), loop: 2189 };
        mario.sounds ['bgm_castle'] = { audio: new Audio ( 'assets/sound/bgm_castle.mp3' ), loop: 69908 };
        mario.sounds ['bgm_overworld'] = { audio: new Audio ( 'assets/sound/bgm_overworld.mp3' ), loop: 1808 };
        mario.sounds ['bgm_athletic'] = { audio: new Audio ( 'assets/sound/bgm_athletic.mp3' ), loop: 3328 };
        mario.sounds ['bgm_underground'] = { audio: new Audio ( 'assets/sound/bgm_underground.mp3' ), loop: 18087 };
        // mario.sounds ['bgm_airship'] = { audio: new Audio ( 'assets/sound/bgm_airship.mp3' ), loop: 8679 };
        mario.sounds ['bgm_airship'] = { audio: new Audio ( 'assets/sound/bgm_airship.wav' ), loop: 4000 };
        mario.sounds ['bgm_ghost'] = { audio: new Audio ( 'assets/sound/bgm_ghost.wav' ), loop: 0 };
        mario.sounds ['bgm_bowser'] = { audio: new Audio ( 'assets/sound/bgm_bowser.mp3' ), loop: 3646 };
        mario.sounds ['bgm_fortress'] = { audio: new Audio ( 'assets/sound/bgm_fortress.mp3' ), loop: 207780 };

        mario.sounds ['bgm_clear'] = { audio: new Audio ( 'assets/sound/bgm_clear.mp3' ), loop: -1 };
        mario.sounds ['bgm_success'] = { audio: new Audio ( 'assets/sound/bgm_success.mp3' ), loop: -1 };
        mario.sounds ['bgm_gameover'] = { audio: new Audio ( 'assets/sound/bgm_gameover.mp3' ), loop: -1 };

        // sfx
        mario.sounds ['snd_jump'] = { audio: new Audio ( 'assets/sound/snd_jump.mp3' ), loop: -1 };
        mario.sounds ['snd_life'] = { audio: new Audio ( 'assets/sound/snd_life.mp3' ), loop: -1 };
        mario.sounds ['snd_hit'] = { audio: new Audio ( 'assets/sound/snd_hit.mp3' ), loop: -1 };
        mario.sounds ['snd_buff'] = { audio: new Audio ( 'assets/sound/snd_buff.mp3' ), loop: -1 };
        mario.sounds ['snd_deathtrigger'] = { audio: new Audio ( 'assets/sound/snd_deathtrigger.mp3' ), loop: -1 };
        mario.sounds ['snd_death'] = { audio: new Audio ( 'assets/sound/snd_death.mp3' ), loop: -1 };
        mario.sounds ['snd_fireball'] = { audio: new Audio ( 'assets/sound/snd_fireball.mp3' ), loop: -1 };
        mario.sounds ['snd_stomp'] = { audio: new Audio ( 'assets/sound/snd_stomp.mp3' ), loop: -1 };
        mario.sounds ['snd_bump'] = { audio: new Audio ( 'assets/sound/snd_bump.mp3' ), loop: -1 };
        mario.sounds ['snd_cannon'] = { audio: new Audio ( 'assets/sound/snd_cannon.mp3' ), loop: -1 };
        mario.sounds ['snd_hammer'] = { audio: new Audio ( 'assets/sound/snd_hammer.mp3' ), loop: -1 };
        mario.sounds ['snd_brick'] = { audio: new Audio ( 'assets/sound/snd_brick.mp3' ), loop: -1 };
        mario.sounds ['snd_itemspawn'] = { audio: new Audio ( 'assets/sound/snd_itemspawn.mp3' ), loop: -1 };
        mario.sounds ['snd_fire'] = { audio: new Audio ( 'assets/sound/snd_fire.mp3' ), loop: -1 };
        mario.sounds ['snd_thwomp'] = { audio: new Audio ( 'assets/sound/snd_thwomp.mp3' ), loop: -1 };
        mario.sounds ['snd_bridge'] = { audio: new Audio ( 'assets/sound/snd_bridge.wav' ), loop: -1 };
        mario.sounds ['snd_fall'] = { audio: new Audio ( 'assets/sound/snd_fall.mp3' ), loop: -1 };
        mario.sounds ['snd_thunder'] = { audio: new Audio ( 'assets/sound/snd_thunder.mp3' ), loop: -1 };
        mario.sounds ['snd_thunder_alt'] = { audio: new Audio ( 'assets/sound/snd_thunder_alt.wav' ), loop: -1 };
        mario.sounds ['snd_smasher'] = { audio: new Audio ( 'assets/sound/snd_smasher.mp3' ), loop: -1 };


        for ( var i = 0; i < 10; ++i )
            mario.sounds ['snd_kick_' + i] = { audio: new Audio ( 'assets/sound/snd_kick.mp3' ), loop: -1 };

        for ( var i = 0; i < 15; ++i )
            mario.sounds ['snd_coin_' + i] = { audio: new Audio ( 'assets/sound/snd_coin.mp3' ), loop: -1 };

        for ( var k in mario.sounds )
        {
            mario.loadingTarget++;
            mario.sounds [k].audio.addEventListener ( 'canplaythrough', function ( ) { mario.loadingProgress++; } );
        }
    },

    initializeResources: function ( )
    {
        mario.loadingTarget += 7;
        mario.tileset = new Image ( );
        mario.tileset.src = 'assets/tileset.png';

        mario.tileset.addEventListener ( 'load', function ( event )
        {
            mario.tilesetWidth = mario.tileset.width;
            mario.tilesetHeight = mario.tileset.height;

            mario.tilesetDimX = Math.floor ( mario.tilesetWidth / 32 );
            mario.tilesetDimY = Math.floor ( mario.tilesetHeight / 32 );

            mario.loadingProgress++;
        } );

        mario.marioIcon = new Image ( );
        mario.marioIcon.src = 'assets/mario.png';
        mario.marioIcon.addEventListener ( 'load', function ( event ) { mario.loadingProgress++; } );

        mario.coinIcon = new Image ( );
        mario.coinIcon.src = 'assets/coin.png';
        mario.coinIcon.addEventListener ( 'load', function ( event ) { mario.loadingProgress++; } );

        mario.spritefont = new Image ( );
        mario.spritefont.src = 'assets/font.png';
        mario.spritefont.addEventListener ( 'load', function ( event ) { mario.loadingProgress++; } );

        // little bricks
        mario.brickTexture = new Image ( );
        mario.brickTexture.src = 'assets/brick.png';
        mario.brickTexture.addEventListener ( 'load', function ( event ) { mario.loadingProgress++; } );

        // rain
        mario.rainTexture = new Image ( );
        mario.rainTexture.src = 'assets/rain.png';
        mario.rainTexture.addEventListener ( 'load', function ( event ) { mario.loadingProgress++; } );

        // combo
        mario.comboParticlesTileset = new Image ( );
        mario.comboParticlesTileset.src = 'assets/combo.png';
        mario.comboParticlesTileset.addEventListener ( 'load', function ( event ) { mario.loadingProgress++; } );

        // initialize backgrounds
        for ( var i = 0; i <= 16; ++i )
        {
            mario.loadingTarget++;

            mario.backgrounds [i] = new Image ( );
            mario.backgrounds [i].src = 'assets/backgrounds/background' + i + '.png';

            mario.backgrounds [i].addEventListener ( 'load', function ( event ) { mario.loadingProgress++; } );
        }

        mario.randomBackgroundNumber = Math.floor ( Math.random ( ) * 4 );

        mario.logo = new Image ( );
        mario.logo.src = 'assets/logo.png';
        mario.logo.addEventListener ( 'load', function ( event ) { mario.loadingProgress++; } );

        mario.initializeAnimations ( );
        mario.initializeEntities ( );
        mario.initializeProgression ( );
        mario.initializeSounds ( );
    },

    initialize: function ( onlyEngine )
    {
        if ( onlyEngine ) mario.onlyEngine = true;
        else mario.onlyEngine = false;

        // load the resources
        mario.initializeResources ( );

        // initialize events
        document.addEventListener ( 'keydown', function ( event )
        {
            if ( event.keyCode == 38 ) mario.keyPressed.up = true; // arrow up
            if ( event.keyCode == 37 ) mario.keyPressed.left = true; // arrow left
            if ( event.keyCode == 39 ) mario.keyPressed.right = true; // arrow right
            if ( event.keyCode == 40 ) mario.keyPressed.down = true; // arrow down
            if ( event.keyCode == 80 ) mario.keyPressed.pause = true; // P
            if ( event.keyCode == 16 ) mario.keyPressed.sprint = true; // Left Shift
            if ( event.keyCode == 32 ) mario.keyPressed.space = true; // Space
            if ( event.keyCode == 192 ) mario.keyPressed.debug = true; // debug
            if ( event.keyCode == 77 ) mario.keyPressed.menu = true;
 
            if ( [16, 32, 37, 38, 39, 40, 80, 77, 192].indexOf ( event.keyCode ) != -1 )
            {
                event.preventDefault ( );
            }
        } );

        document.addEventListener ( 'keyup', function ( event )
        {
            if ( event.keyCode == 38 ) mario.keyPressed.up = false;
            if ( event.keyCode == 37 ) mario.keyPressed.left = false;
            if ( event.keyCode == 39 ) mario.keyPressed.right = false;
            if ( event.keyCode == 40 ) mario.keyPressed.down = false;
            if ( event.keyCode == 80 ) mario.keyPressed.pause = false;
            if ( event.keyCode == 16 ) mario.keyPressed.sprint = false;
            if ( event.keyCode == 32 ) mario.keyPressed.space = false;
            if ( event.keyCode == 192 ) mario.keyPressed.debug = false;
            if ( event.keyCode == 77 ) mario.keyPressed.menu = false;
 
            if ( [16, 32, 37, 38, 39, 40, 80, 77, 192].indexOf ( event.keyCode ) != -1 )
            {
                event.preventDefault ( );
            }
        } );

        // initialize the canvas and rendering context
        mario.canvas = document.querySelector ( '#screen' );
        mario.canvas.addEventListener ( 'click', mario.onClick );
        mario.canvas.addEventListener ( 'mousemove', mario.onMouseMove );

        if ( !mario.canvas.getContext )
        {
            throw new Error ( 'Unsupported Browser: Cannot initialize canvas!' );
        }

        mario.context = mario.canvas.getContext ( '2d', { alpha: false, antialias: false } );        
        window.requestAnimationFrame ( mario.update );

        mario.gameStartTimestamp = new Date ( );
    }
};