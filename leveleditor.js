/*
 * Mario: The javascript Mario (a.k.a. how long will it take for nintento to take it down)
 *
 * About: The script is pretty self-explanatory: it's just a mario game
 * Author: etheryt@protonmail.com
 * 
 * DISCLAIMER: EVERY ASSET IN THIS GAME BELONGS TO NINTENDO
 */

var objectMapping = [
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 
    11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 
    22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 
    33, 34, 35, 36, 37, 38, 39, 47, 49, 50, 51, 
    52, 53, 54, 55, 57, 59, 60, 61, 64, 65, 66
];

var marioeditor =
{
    // fps limits
    previousDelta: 0,
    fpsLimit: 60,
    editorActive: false,

    project: undefined,
    currentView: 0,

    level: undefined,
    canvas: undefined,
    context: undefined,
    tileset: undefined,
    miniatures: undefined,

    animations: {},
    templates: undefined,

    isMenuOpen: false,
    isObjectMenuOpen: false,
    menuToggleTimeout: 0,
    levelSwitchTimeout: 0,

    keyPressed:
    {
        up: false,
        down: false,
        left: false,
        right: false,
        menu: false,
        objects: false,
        rotate: false,
        tab: false,
        settings: false
    },

    tilesetWidth: 0,
    tilesetHeight: 0,
    tilesetDimX: 0,
    tilesetDimY: 0,

    tileSelectionWidth: 11,
    tileSelectionHeight: 6,
    tileSelectionPages: 0,
    currentPage: 0,

    heldItem:
    {
        type: 'none',
        data: undefined
    },

    cursorX: 0,
    cursorY: 0,

    backgrounds: [],
    interfaceOverlay: undefined,
    levelPresets: [ 6, 132, 6, 32, 0, 0, 32 ],

    // since the camera will only follow in one axis
    // we only use x coordinate here
    cameraOverworld: { x: 0 },
    cameraBonus: { x: 0 },

    camera: undefined,
    levelEditTimestamp: undefined,

    renderObject: function ( x, y, object )
    {
        if ( !marioeditor.templates ) return;
        if ( marioeditor.templates.length - 1 < object ) return;

        var dimx = marioeditor.templates [object].dimensions [0];
        var dimy = marioeditor.templates [object].dimensions [1];

        var dx = x - marioeditor.templates [object].origin [0];
        var dy = y - marioeditor.templates [object].origin [1];

        var name = marioeditor.templates [object].name;
        var anim = marioeditor.templates [object].anim;

        if ( !marioeditor.animations [anim] ) return;

        marioeditor.context.drawImage ( marioeditor.animations [anim], 0, 0, dimx, dimy, dx, dy, dimx, dimy );
    },

    renderMiniature: function ( x, y, object )
    {
        if ( object < 0 || object > 54 ) return;

        var minX = ( object % 11 ) * 32;
        var minY = ( Math.floor ( object / 11 ) ) * 32;

        marioeditor.context.drawImage ( marioeditor.miniatures, minX, minY, 32, 32, x, y, 32, 32 );
    },

    renderTile: function ( x, y, index )
    {
        if ( !marioeditor.context )
        {
            throw new Error ( 'Cannot render tile: Context not initialized!' );
        }

        index = index - 1;
        if ( index < 0 || index > marioeditor.tilesetDimX * marioeditor.tilesetDimY ) return;

        var tileX = ( index % marioeditor.tilesetDimX ) * 32;
        var tileY = ( Math.floor ( index / marioeditor.tilesetDimX ) ) * 32;

        marioeditor.context.drawImage ( marioeditor.tileset, tileX, tileY, 32, 32, x, y, 32, 32 );
    },

    renderCursor: function ( )
    {
        if ( marioeditor.heldItem.type == 'tile' )
        {
            var index = marioeditor.heldItem.data.index;
            marioeditor.renderTile ( marioeditor.cursorX - 10, marioeditor.cursorY - 10, index );
        }
        else if ( marioeditor.heldItem.type == 'object' )
        {
            var id = marioeditor.heldItem.data.id;
            marioeditor.renderObject ( marioeditor.cursorX, marioeditor.cursorY, id );
        }
    },

    renderBackground: function ( )
    {
        var currentBackground = marioeditor.level.background;

        if ( marioeditor.backgrounds.length >= currentBackground + 1 )
        {
            var backgroundWidth = marioeditor.backgrounds [currentBackground].width;
            var backgroundX = Math.floor ( marioeditor.camera.x / 5 );
            
            backgroundX = backgroundX % backgroundWidth;
            backgroundX = -backgroundX;

            marioeditor.context.drawImage ( marioeditor.backgrounds [currentBackground], backgroundX, 0 );

            if ( backgroundX + backgroundWidth < 640 )
                marioeditor.context.drawImage ( marioeditor.backgrounds [currentBackground], backgroundX + backgroundWidth, 0 );

            if ( backgroundX > 0 )
                marioeditor.context.drawImage ( marioeditor.backgrounds [currentBackground], backgroundX - backgroundWidth, 0 );
        }
    },

    pointToIndex: function ( x, y )
    {
        if ( x < 0 || y < 0 || x > marioeditor.level.size || y > 15 )
            return -1;

        return ( marioeditor.level.size * y + x );
    },

    render: function ( )
    {
        // clear the screen before rendering
        // marioeditor.context.clearRect ( 0, 0, marioeditor.canvas.width, marioeditor.canvas.height );

        // this fixes the rendering bug
        marioeditor.context.fillStyle = 'black';
        marioeditor.context.fillRect ( 0, 0, marioeditor.canvas.width, marioeditor.canvas.height );

        if ( marioeditor.isMenuOpen )
        {
            marioeditor.context.fillStyle = '#ffffff';
            marioeditor.context.font = '16px nes';

            marioeditor.context.fillText ( 'Select a Tile Page ' + ( marioeditor.currentPage + 1 ), 160, 50 );
            marioeditor.context.fillText ( 'Prev Page', 75, 430 );
            marioeditor.context.fillText ( 'Next Page', 430, 430 );

            var pageDimensions = marioeditor.tileSelectionWidth * marioeditor.tileSelectionHeight;
            var startingTile = marioeditor.currentPage * pageDimensions;
            var endingTile = startingTile + pageDimensions;

            for ( var i = startingTile + 1; i < endingTile + 1; ++i )
            {
                var dx = ( i - 1 ) % marioeditor.tileSelectionWidth * 48;
                var dy = Math.floor ( ( i - startingTile - 1 ) / marioeditor.tileSelectionWidth ) * 48;

                dx = 63 + dx;
                dy = 100 + dy;

                marioeditor.renderTile ( dx, dy, i );
            }
        }
        else if ( marioeditor.isObjectMenuOpen )
        {
            marioeditor.context.fillStyle = '#ffffff';
            marioeditor.context.font = '16px nes';

            marioeditor.context.fillText ( 'Select an object', 190, 50 );

            for ( var i = 0; i < 55; ++i )
            {
                var dx = i % 11 * 48;
                var dy = Math.floor ( i / 11 ) * 48;

                dx = 63 + dx;
                dy = 100 + dy;

                marioeditor.renderMiniature ( dx, dy, i );
            }
        }
        else
        {
            // render the background
            marioeditor.renderBackground ( );

            var startTile = Math.floor ( marioeditor.camera.x / 32 );

            for ( var y = 0; y < 15; ++y )
            {
                for ( var x = startTile; x < startTile + 21; ++x )
                {
                    if ( x >= marioeditor.level.size ) continue;
                    var index = marioeditor.pointToIndex ( x, y );
                    
                    var tx = x * 32;
                    var ty = y * 32;

                    var tile = marioeditor.level.tiles [index][0];
                    tx -= marioeditor.camera.x;

                    if ( tile == -1 ) // the tile is an object
                    {
                        var objectID = marioeditor.level.tiles [index][1];
                        marioeditor.renderObject ( tx + 16, ty + 16, objectID );
                    }
                    else
                    {
                        marioeditor.renderTile ( tx, ty, tile );

                        if ( tile == 2 && marioeditor.level.tiles [index][1] > 0 )
                        {
                            var itemID = marioeditor.level.tiles [index][1];

                            if ( itemID == 0 ) marioeditor.renderObject ( tx + 16, ty + 16, 14 );
                            else if ( itemID == 6 ) marioeditor.renderObject ( tx + 16, ty + 16, 38 );
                            else if ( itemID == 5 ) marioeditor.renderObject ( tx + 16, ty + 16, 1 );
                            else marioeditor.renderObject ( tx + 16, ty + 16, itemID );
                        }
                    }
                }
            }

            marioeditor.context.fillStyle = '#ff0000';
            marioeditor.context.font = '7px nes';

            // render warp icons
            for ( var i = 0; i < marioeditor.project.warps.length; ++i )
            {
                var warp = marioeditor.project.warps [i];
                if ( warp.level != marioeditor.currentView ) continue;

                var wx = warp.position.x + 16 - marioeditor.camera.x;
                var wy = warp.position.y + 16;

                marioeditor.renderObject ( wx, wy, warp.id );
                
                var initY = -30;
                if ( warp.type == 0 ) initY -= 10;

                marioeditor.context.fillText ( warp.name, wx - 3, wy + initY );

                if ( warp.type == 0 ) 
                {
                    marioeditor.context.fillText ( 'input', wx - 3, wy + initY + 10 );
                    marioeditor.context.fillText ( 'opt:' + warp.output, wx - 10, wy + initY + 20 );
                }
                else marioeditor.context.fillText ( 'output', wx - 3, wy + initY + 10 );
            }

            marioeditor.context.fillStyle = '#ffffff';
            marioeditor.context.font = '14px nes';

            if ( marioeditor.currentView == 0 ) marioeditor.context.fillText ( 'Overworld', 500, 25 );
            else marioeditor.context.fillText ( 'Bonus', 560, 25 );
        }

        // render the cursor
        marioeditor.renderCursor ( );
    },

    frame: function ( timestamp )
    {
        window.requestAnimationFrame ( marioeditor.frame ); // request another frame

        var delta = timestamp - marioeditor.previousDelta;
        if ( delta < 1000 / marioeditor.fpsLimit ) return;

        marioeditor.previousDelta = timestamp;

        if ( !marioeditor.editorActive ) return;
        if ( !marioeditor.level ) return;

        if ( marioeditor.keyPressed.settings )
        {
            marioeditor.keyPressed.settings = false;
            marioeditor.editorActive = false;

            displayOverlay ( 'levelsettings' );
            updateSettingsOverlay ( );
        }

        if ( !marioeditor.camera ) marioeditor.camera = marioeditor.cameraOverworld;

        if ( marioeditor.keyPressed.tab && marioeditor.levelSwitchTimeout <= 0 )
        {
            if ( marioeditor.currentView == 0 )
            {
                marioeditor.level = marioeditor.project.bonus;
                marioeditor.currentView = 1;
                marioeditor.camera = marioeditor.cameraBonus;
            }
            else
            {
                marioeditor.level = marioeditor.project.overworld;
                marioeditor.currentView = 0;
                marioeditor.camera = marioeditor.cameraOverworld;
            }

            marioeditor.levelSwitchTimeout = 30;
        }

        if ( marioeditor.levelSwitchTimeout > 0 ) marioeditor.levelSwitchTimeout--;
        if ( marioeditor.menuToggleTimeout > 0 ) marioeditor.menuToggleTimeout--;

        if ( marioeditor.menuToggleTimeout <= 0 )
        {
            if ( marioeditor.keyPressed.menu && !marioeditor.isObjectMenuOpen )
            {
                marioeditor.menuToggleTimeout = 30;
                marioeditor.keyPressed.menu = false;
                marioeditor.isMenuOpen = !marioeditor.isMenuOpen;
            }

            if ( marioeditor.keyPressed.objects && !marioeditor.isMenuOpen )
            {
                marioeditor.menuToggleTimeout = 30;
                marioeditor.keyPressed.objects = false;
                marioeditor.isObjectMenuOpen = !marioeditor.isObjectMenuOpen;
            }
        }

        if ( !marioeditor.isMenuOpen )
        {
            // camera movement
            if ( marioeditor.keyPressed.right ) marioeditor.camera.x += 4;
            if ( marioeditor.keyPressed.left ) marioeditor.camera.x -= 4;

            if ( marioeditor.camera.x < 0 ) marioeditor.camera.x = 0;

            if ( marioeditor.camera.x > ( marioeditor.level.size - 20 ) * 32 ) 
                marioeditor.camera.x = ( marioeditor.level.size - 20 ) * 32;
        }

        marioeditor.render ( );
    },

    intersects: function ( x, y, w, h, dx, dy, dw, dh )
    {
        if ( x <= ( dw + dx ) && ( x + w ) >= dx && y <= ( dy + dh ) && ( y + h ) >= dy )
        {
            return true;
        }

        return false;
    },

    onMouseMove: function ( event )
    {
        var rect = marioeditor.canvas.getBoundingClientRect ( );

        var x = event.clientX - rect.left;
        var y = event.clientY - rect.top;

        var scale = 640 / rect.width;

        x = x * scale;
        y = y * scale;

        marioeditor.cursorX = x;
        marioeditor.cursorY = y;
    },

    onClick: function ( event )
    {
        if ( !marioeditor.level || !marioeditor.editorActive ) return;
        
        var rect = marioeditor.canvas.getBoundingClientRect ( );

        var x = event.clientX - rect.left;
        var y = event.clientY - rect.top;

        var scale = 640 / rect.width;

        x = x * scale;
        y = y * scale;

        if ( marioeditor.isMenuOpen )
        {
            // previous page button
            if ( marioeditor.intersects ( x, y, 1, 1, 78, 410, 140, 20 ) )
            {
                marioeditor.currentPage--;
                if ( marioeditor.currentPage < 0 ) marioeditor.currentPage = marioeditor.tileSelectionPages;
            }

            // next page button
            if ( marioeditor.intersects ( x, y, 1, 1, 430, 410, 140, 20 ) )
            {
                marioeditor.currentPage++;
                if ( marioeditor.currentPage > marioeditor.tileSelectionPages ) marioeditor.currentPage = 0;
            }

            if ( marioeditor.intersects ( x, y, 1, 1, 63, 100, 
                marioeditor.tileSelectionWidth * 48, marioeditor.tileSelectionHeight * 48 ) )
            {
                // coords relative to tile list rect
                var rx = x - 63;
                var ry = y - 100;

                // check if hits tile
                if ( rx % 48 <= 32 && ry % 48 <= 32 )
                {
                    // tile coords
                    var tx = Math.floor ( rx / 48 );
                    var ty = Math.floor ( ry / 48 );

                    var pageDimensions = marioeditor.tileSelectionWidth * marioeditor.tileSelectionHeight
                    var startingTile = marioeditor.currentPage * pageDimensions;

                    var newIndex = startingTile + ( ty * marioeditor.tileSelectionWidth + tx );

                    if ( marioeditor.heldItem.type != 'tile' )
                    {
                        marioeditor.heldItem.type = 'tile';
                        marioeditor.heldItem.data = { index: newIndex + 1 };
                    } 
                    else 
                    { 
                        marioeditor.heldItem.type = 'none'; 
                    }
                }
            }
        }
        else if ( marioeditor.isObjectMenuOpen )
        {
            // object edit code - pretty much the same as tile code
            if ( marioeditor.intersects ( x, y, 1, 1, 63, 100, 11 * 48, 5 * 48 ) )
            {
                // coords relative to tile list rect
                var rx = x - 63;
                var ry = y - 100;

                // check if hits tile
                if ( rx % 48 <= 32 && ry % 48 <= 32 )
                {
                    // icon coords
                    var tx = Math.floor ( rx / 48 );
                    var ty = Math.floor ( ry / 48 );

                    var newId = ty * 11 + tx;
                    newId = objectMapping [newId];

                    if ( marioeditor.heldItem.type != 'object' )
                    {
                        marioeditor.heldItem.type = 'object';
                        marioeditor.heldItem.data = { id: newId };
                    } 
                    else 
                    { 
                        marioeditor.heldItem.type = 'none'; 
                    }
                }
            }
        }
        else
        {
            // get position relative to world origin
            var rx = marioeditor.camera.x + x;
            var ry = y;

            // convert to tile coordinates
            var tx = Math.floor ( rx / 32 );
            var ty = Math.floor ( ry / 32 );

            var index = marioeditor.pointToIndex ( tx, ty );

            if ( marioeditor.heldItem.type == 'tile' )
            {
                if ( marioeditor.level.tiles [index][0] != 0 )
                {
                    marioeditor.level.tiles [index] = [ 0, 0 ];
                }
                else
                {
                    marioeditor.level.tiles [index][0] = marioeditor.heldItem.data.index;
                }
            }
            else if ( marioeditor.heldItem.type == 'object' )
            {
                if ( marioeditor.heldItem.data.id >= 32 && marioeditor.heldItem.data.id <= 35 )
                    return ( marioeditor.createWarp ( tx, ty, marioeditor.heldItem.data.id ) );

                if ( marioeditor.level.tiles [index][0] != 0 )
                {
                    if ( marioeditor.level.tiles [index][0] == 2 && [1, 2, 3, 4, 38].indexOf ( marioeditor.heldItem.data.id ) != -1 )
                    {
                        if ( marioeditor.heldItem.data.id == 1 ) marioeditor.level.tiles [index][1] = 5;
                        else if ( marioeditor.heldItem.data.id == 38 ) marioeditor.level.tiles [index][1] = 6;
                        else marioeditor.level.tiles [index][1] = marioeditor.heldItem.data.id;
                    }
                    else
                    {
                        marioeditor.level.tiles [index] = [ 0, 0 ];
                    }
                }
                else
                {
                    var value = -1; // -1 represents an object

                    marioeditor.level.tiles [index][0] = value;
                    marioeditor.level.tiles [index][1] = marioeditor.heldItem.data.id; // data sector is object type

                    console.log ( 'object placed on ' + tx + ',' + ty + ' with id ' + marioeditor.level.tiles [index][1] + ' and value ' + value );
                }
            }
        }

        event.preventDefault ( );
    },

    intersects: function ( x, y, w, h, dx, dy, dw, dh )
    {
        if ( x <= ( dw + dx ) && ( x + w ) >= dx && y <= ( dy + dh ) && ( y + h ) >= dy )
        {
            return true;
        }

        return false;
    },

    createWarp: function ( tx, ty, id )
    {
        for ( var i = 0; i < marioeditor.project.warps.length; ++i )
        {
            var wi = marioeditor.project.warps [i];
            var entity = marioeditor.templates [wi.id];

            if ( marioeditor.intersects ( tx * 32, ty * 32, 32, 32, wi.position.x - entity.origin [0], wi.position.y - entity.origin [1], 32, 32 ) && wi.level == marioeditor.currentView )
                return;
        }

        displayWarpModal ( { tx: tx, ty: ty, id: id }, function ( data )
        {
            var warp = 
            {
                name: data.warpName,
                type: data.warpType,
                output: data.warpOutputName,
                level: marioeditor.currentView,
                position: { x: ( tx * 32 ), y: ( ty * 32 ) },
                id: id
            };

            for ( var i = 0; i < marioeditor.project.warps.length; ++i )
            {
                var wi = marioeditor.project.warps [i];

                if ( wi.name == data.warpName )
                {
                    alert ( 'Warp with name ' + wi.name + ' already exists!' );
                    return;
                }
            }

            marioeditor.project.warps.push ( warp );
        } );
    },

    deleteLevel: function ( )
    {
        marioeditor.level = undefined;
        marioeditor.editorActive = false;
    },

    generateLevel: function ( size, defaulttile, music, background )
    {
        // create an empty level
        var level = 
        {
            tiles: [],
            size: size,
            music: music,
            background: background
        };

        for ( var i = 0; i < size * 15; ++i ) // fill the level buffer
        {
            if ( Math.floor ( i / size ) == 14 ) // we want floor to be the default tile
                level.tiles.push ( [ defaulttile, 0 ] );
            else
                level.tiles.push ( [ 0, 0 ] );
        }

        return level;
    },

    createLevel: function ( overworldSize, overworldPreset, overworldMusic, bonusSize, bonusPreset, bonusMusic )
    {
        var defaultOverworldTile = marioeditor.levelPresets [ overworldPreset ];
        var defaultBonusTile = marioeditor.levelPresets [ bonusPreset ];

        var overworld = marioeditor.generateLevel ( overworldSize, defaultOverworldTile, overworldMusic, overworldPreset );
        var bonus = marioeditor.generateLevel ( bonusSize, defaultBonusTile, bonusMusic, bonusPreset );

        var project = 
        {
            overworld: overworld,
            bonus: bonus,
            warps: [ ]
        };

        marioeditor.project = project;
        marioeditor.level = marioeditor.project.overworld;
    },

    exportLevel: function ( )
    {
        return JSON.stringify ( marioeditor.project );
    },

    importLevel: function ( data )
    {
        var parsed = { };

        try
        {
            parsed = JSON.parse ( data );

            marioeditor.project = parsed;
            marioeditor.level = marioeditor.project.overworld;
        } 
        catch ( e ) 
        { 
            throw new Error ( 'Failed to parse level data!' ); 
        }
    },

    initializeAnimations: function ( )
    {
        // load all requested animations
        for ( var i = 0; i < marioeditor.templates.length; ++i )
        {
            var anim = marioeditor.templates [i].anim;
            var animSrc = 'assets/entities/' + anim + '.png';

            marioeditor.animations [anim] = new Image ( );
            marioeditor.animations [anim].src = animSrc;
        }
    },

    initializeEntities: function ( )
    {
        var xhr = new XMLHttpRequest ( );

        xhr.addEventListener ( 'readystatechange', function ( event )
        {
            if ( this.readyState == 4 )
            {
                if ( this.status == 200 )
                {
                    marioeditor.templates = JSON.parse ( this.responseText );
                    marioeditor.initializeAnimations ( );
                }
                else
                {
                    throw new Error ( 'Failed to load entity database!' );
                }
            }
        } );

        xhr.open ( 'GET', 'entities.json' );
        xhr.send ( );
    },

    initializeResources: function ( )
    {
        marioeditor.tileset = new Image ( );
        marioeditor.tileset.src = 'assets/tileset.png';

        marioeditor.tileset.addEventListener ( 'load', function ( event )
        {
            marioeditor.tilesetWidth = marioeditor.tileset.width;
            marioeditor.tilesetHeight = marioeditor.tileset.height;

            marioeditor.tilesetDimX = Math.floor ( marioeditor.tilesetWidth / 32 );
            marioeditor.tilesetDimY = Math.floor ( marioeditor.tilesetHeight / 32 );

            var tiles = marioeditor.tilesetDimX * marioeditor.tilesetDimY;
            var pages = Math.ceil ( tiles / ( marioeditor.tileSelectionWidth * marioeditor.tileSelectionHeight ) );

            marioeditor.tileSelectionPages = pages;
        } );

        marioeditor.miniatures = new Image ( );
        marioeditor.miniatures.src = 'assets/entities/icons.png';

        // initialize backgrounds
        for ( var i = 0; i <= 6; ++i )
        {
            marioeditor.backgrounds [i] = new Image ( );
            marioeditor.backgrounds [i].src = 'assets/backgrounds/background' + i + '.png';
        }

        marioeditor.initializeEntities ( );
    },

    initialize: function ( )
    {
        // load the resources
        marioeditor.initializeResources ( );
        marioeditor.editorActive = true;

        // initialize events
        document.addEventListener ( 'keydown', function ( event )
        {
            if ( !marioeditor.editorActive ) return;

            if ( event.keyCode == 87 ) marioeditor.keyPressed.up = true; // W
            if ( event.keyCode == 65 ) marioeditor.keyPressed.left = true; // A
            if ( event.keyCode == 68 ) marioeditor.keyPressed.right = true; // D
            if ( event.keyCode == 83 ) marioeditor.keyPressed.down = true; // S
            if ( event.keyCode == 77 ) marioeditor.keyPressed.menu = true; // M
            if ( event.keyCode == 66 ) marioeditor.keyPressed.objects = true; // B
            if ( event.keyCode == 9 ) marioeditor.keyPressed.tab = true;
            if ( event.keyCode == 78 ) marioeditor.keyPressed.settings = true; // N
            // if ( event.keyCode == 86 ) marioeditor.keyPressed.rotate = true; // V
 
            if ( [78, 9, 87, 65, 68, 83, 77, 66].indexOf ( event.keyCode ) != -1 )
            {
                event.preventDefault ( );
            }
        } );

        document.addEventListener ( 'keyup', function ( event )
        {
            if ( !marioeditor.editorActive ) return;

            if ( event.keyCode == 87 ) marioeditor.keyPressed.up = false;
            if ( event.keyCode == 65 ) marioeditor.keyPressed.left = false;
            if ( event.keyCode == 68 ) marioeditor.keyPressed.right = false;
            if ( event.keyCode == 83 ) marioeditor.keyPressed.down = false;
            if ( event.keyCode == 77 ) marioeditor.keyPressed.menu = false;
            if ( event.keyCode == 66 ) marioeditor.keyPressed.objects = false;
            if ( event.keyCode == 9 ) marioeditor.keyPressed.tab = false;
            if ( event.keyCode == 78 ) marioeditor.keyPressed.settings = false;
            // if ( event.keyCode == 86 ) marioeditor.keyPressed.rotate = false;

            if ( [78, 9, 87, 65, 68, 83, 77, 66].indexOf ( event.keyCode ) != -1 )
            {
                event.preventDefault ( );
            }
        } );

        // initialize the canvas and rendering context
        marioeditor.canvas = document.querySelector ( '#editorscreen' );
        marioeditor.interfaceOverlay = document.querySelector ( '#overlay' );

        marioeditor.canvas.addEventListener ( 'click', marioeditor.onClick );
        marioeditor.canvas.addEventListener ( 'mousemove', marioeditor.onMouseMove );

        if ( !marioeditor.canvas.getContext )
        {
            throw new Error ( 'Unsupported Browser: Cannot initialize canvas!' );
        }

        marioeditor.context = marioeditor.canvas.getContext ( '2d', { alpha: false, antialias: false } );        
        window.requestAnimationFrame ( marioeditor.frame );

        marioeditor.levelEditTimestamp = new Date ( );
    }
};