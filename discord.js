const discordrpc = require ( 'discord-rpc' );
const rpc = new discordrpc.Client ( { transport: 'ipc' } );

const clientId = '568834992977739816';

function getBackgroundName ( id )
{
    var bg = [ 'overworld', 'underground', 'athletic', 'castle', 'ghost', 'airship', 'fortress', 'overworld', 'overworld', 'overworld', 'overworld', 'airship', 'airship', 'airship', 'fortress', 'fortress', 'fortress' ];

    return bg [id];
}

function setActivity ( )
{
    if ( typeof marioeditor !== 'undefined' )
    {
        if ( marioeditor.project && marioeditor.project.overworld )
        {
            rpc.setActivity ( {
                details: 'Creating a level',
                startTimestamp: Math.round ( marioeditor.levelEditTimestamp.getTime ( ) / 1000 ),
                largeImageKey: getBackgroundName ( marioeditor.project.overworld.background ),
                largeImageText: getBackgroundName ( marioeditor.project.overworld.background ),
                instance: false
            } );
        }
        else
        {
            rpc.setActivity ( {
                details: 'In the menu',
                largeImageKey: getBackgroundName ( mario.randomBackgroundNumber ),
                largeImageText: 'menu',
                instance: false
            } );
        }
    }
    else
    {
        if ( mario.isLevelLoaded )
        {
            var levelName = mario.progression.progression [mario.progression.currentLevel].displayName.toUpperCase ( );
            var lives = mario.gamemode.lives;
            var background = mario.level.background;

            rpc.setActivity ( {
                details: 'Playing Level ' + levelName,
                state: 'Lives left: ' + lives,
                startTimestamp: Math.round ( mario.gameStartTimestamp.getTime ( ) / 1000 ),
                largeImageKey: getBackgroundName ( background ),
                largeImageText: getBackgroundName ( background ),
                instance: false
            } );
        }
        else
        {
            rpc.setActivity ( {
                details: 'In the menu',
                largeImageKey: getBackgroundName ( mario.randomBackgroundNumber ),
                largeImageText: 'menu',
                instance: false
            } );
        }
    }
}

var rpcInterval = undefined;

window.onunload = function ( )
{
    rpc.destroy ( );
    clearInterval ( rpcInterval );
}

rpc.on ( 'ready', function ( )
{
	var startTimestamp = new Date ( );
    console.log ( 'discord rpc was initialized' );
    
    setActivity ( );
	
	rpcInterval = setInterval ( function ( )
	{		
		setActivity ( );
	}, 15e3 ); // 15 s interval
} );

rpc.login ( { clientId } );