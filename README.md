public version of my old javascript mario game, stripped of assets as they belong to nintendo, it is based on javascript and can be ran in browser with slight modifications (removing node-dependent files) or using nw.js.

# how to build #
1. download nw.js from [here](https://nwjs.io/)
2. run the project with downloaded or compiled version of nw.js

# author #
the author of this project is me (etheryt@protonmail.com), my discord tag is `eter#3240`