/*
 * Simple and primitive debugging module
 * Should be used on dev builds
 * Features:
 *  - level editor
 *  - in-level debug menu
 *  - level editor test mode
 */

var mariodebug =
{
    debugScripts:
    {
        fullLives:
        {
            name: "full lives",
            handler: function ( )
            {
                mario.gamemode.lives = 99;
            }
        },

        powerup:
        {
            name: "power up",
            handler: function ( )
            {
                mario.playerPowerup ( 2 );
            }
        },

        advance:
        {
            name: "progress success",
            handler: function ( )
            {
                mario.progressionSuccess ( );
            }
        },

        butcher:
        {
            name: "kill everyone",
            handler: function ( )
            {
                mario.destroyAllEnemies ( );
            }
        },

        toggleGodMode:
        {
            name: "enable god mode",
            handler: function ( )
            {
                if ( mario.godMode ) mariodebug.debugScripts.toggleGodMode.name = 'enable god mode';
                else mariodebug.debugScripts.toggleGodMode.name = 'disable god mode';

                document.querySelector ( '#option-toggleGodMode' ).innerHTML = mariodebug.debugScripts.toggleGodMode.name;

                mario.godMode = !mario.godMode;
            }
        },

        toggleDebugDraw:
        {
            name: "enable debug overlay",
            handler: function ( )
            {
                if ( mario.displayHitboxes ) mariodebug.debugScripts.toggleDebugDraw.name = 'enable debug overlay';
                else mariodebug.debugScripts.toggleDebugDraw.name = 'disable debug overlay';

                document.querySelector ( '#option-toggleDebugDraw' ).innerHTML = mariodebug.debugScripts.toggleDebugDraw.name;

                mario.displayHitboxes = !mario.displayHitboxes;
            }
        },

        closeDebug:
        {
            name: "close debug",
            handler: function ( )
            {
                mariodebug.closeDebugMenu ( );
            }
        }
    },

    editor:
    {
        testLevel: function ( )
        {
            document.querySelector ( '#editorscreen' ).style.display = 'none';
            document.querySelector ( '#screen' ).style.display = 'block';

            marioeditor.editorActive = false;
            mario.loadLevelRaw ( marioeditor.exportLevel ( ) );
            mario.isPaused = false;

            mario.gamemode.lives = 5;

            mario.progression = 
            {
                lives: 5,
                progression: [ { levelName: "test", displayName: "test", levelType: 0, time: 300 } ]
            };
        },

        displayEditor: function ( )
        {
            document.querySelector ( '#editorscreen' ).style.display = 'block';
            document.querySelector ( '#screen' ).style.display = 'none';

            marioeditor.editorActive = true;
            mario.isLevelLoaded = false;

            // unloading code
            mario.unloadLevel ( );
            mario.level = { };
            mario.world = { };
        },

        initialize: function ( )
        {
            console.log ( 'initializing debug link between editor and game engine...' );

            mario.hooks.onLevelFail = function ( )
            {
                mariodebug.editor.displayEditor ( );
            }

            mario.hooks.onLevelSuccess = function ( )
            {
                mariodebug.editor.displayEditor ( );
            }
        }
    },

    displayDebugMenu: function ( )
    {
        document.querySelector ( '.debugMenuOverlay' ).style.display = 'flex';

        // initialize debug options
        for ( var option in mariodebug.debugScripts )
        {
            var node = document.createElement ( 'LI' );
            node.addEventListener ( 'click', mariodebug.debugScripts [option].handler );
            node.innerHTML = mariodebug.debugScripts [option].name;
            node.id = 'option-' + option;

            document.querySelector ( '#debugMenuContent' ).appendChild ( node );
        }
    },

    closeDebugMenu: function ( )
    {
        document.querySelector ( '.debugMenuOverlay' ).style.display = 'none';
        document.querySelector ( '#debugMenuContent' ).innerHTML = '';
        mario.isFullyPaused = false;
    }
};