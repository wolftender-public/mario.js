// individual ui layer scripts
// to clean up leveleditor.js
var pickers = { };
var levelPresets = [ 'overworld', 'underground', 'athletic', 'castle', 'ghost', 'airship', 'fortress' ];
var musicPresets = [ 'overworld', 'underground', 'athletic', 'airship', 'castle', 'ghost', 'bowser', 'fortress' ];
var warpDialogCallback = undefined;
var selectedWarpType = 0;

var currentPreset = 
{  
    overworld: { theme: 0, music: 0 },
    bonus: { theme: 0, music: 0 }
};

function initializePicker ( id, data, callback )
{
    var element = document.getElementById ( id );
    element.classList.add ( 'picker' );

    var left = document.createElement ( 'div' );
    left.classList.add ( 'left' );
    left.innerHTML = '◀';

    left.addEventListener ( 'click', function ( )
    {
        var current = pickers [id];
        current.selection--;
        if ( current.selection < 0 ) current.selection = current.data.length - 1;
        current.content.innerHTML = current.data [current.selection];
        current.callback ( current );
    } );

    var right = document.createElement ( 'div' );
    right.classList.add ( 'right' );
    right.innerHTML = '▶';

    right.addEventListener ( 'click', function ( )
    {
        var current = pickers [id];
        current.selection++;
        if ( current.selection > current.data.length - 1 ) current.selection = 0;
        current.content.innerHTML = current.data [current.selection];
        current.callback ( current );
    } );

    var content = document.createElement ( 'div' );
    content.classList.add ( 'content' );
    content.innerHTML = data [0];
    
    var picker = { id: id, data: data, callback: callback, selection: 0, content: content };
    pickers [id] = picker;

    element.appendChild ( left );
    element.appendChild ( content );
    element.appendChild ( right );

    callback ( pickers [id] );
}

function updatePicker ( id, selection )
{
    if ( pickers.hasOwnProperty ( id ) )
    {
        var current = pickers [id];
        current.selection = selection % ( current.data.length );
        current.content.innerHTML = current.data [current.selection];
        current.callback ( current );
    }
}

function presetPickerCallback ( picker )
{
    var option = picker.selection;
    var optionName = picker.data [picker.selection];

    var isSettings = ( picker.id.substring ( 0, 8 ) == 'settings' );

    if ( picker.id == 'overworldPicker' || picker.id == 'settingsOverworldPicker' )
    {
        currentPreset.overworld.theme = option;

        if ( !isSettings ) document.getElementById ( 'overworldPreview' ).src = 'assets/editor/' + optionName + '.png';
        else document.getElementById ( 'settingsOverworldPreview' ).src = 'assets/editor/' + optionName + '.png';
    }
    else
    {
        currentPreset.bonus.theme = option;

        if ( !isSettings ) document.getElementById ( 'bonusPreview' ).src = 'assets/editor/' + optionName + '.png';
        else document.getElementById ( 'settingsBonusPreview' ).src = 'assets/editor/' + optionName + '.png';
    }
}

function musicPickerCallback ( picker ) 
{ 
    var option = picker.selection;

    if ( picker.id == 'overworldMusicPicker' || picker.id == 'settingsOverworldMusicPicker' ) 
        currentPreset.overworld.music = option;
    else 
        currentPreset.bonus.music = option;
}

function warpPickerCallback ( picker )
{
    selectedWarpType = picker.selection;

    if ( picker.data [picker.selection] == 'input' )
        document.getElementById ( 'inputWarpOptions' ).style.display = 'block';
    else
        document.getElementById ( 'inputWarpOptions' ).style.display = 'none';
}

function exitOverlay ( )
{
    marioeditor.editorActive = true;
    clearOverlay ( ); 
}

function initializeEditorLayers ( )
{
    displayOverlay ( 'leveloptions' );

    initializePicker ( 'overworldPicker', levelPresets, presetPickerCallback );
    initializePicker ( 'bonusPicker', levelPresets, presetPickerCallback );
    initializePicker ( 'overworldMusicPicker', musicPresets, musicPickerCallback );
    initializePicker ( 'bonusMusicPicker', musicPresets, musicPickerCallback );

    initializePicker ( 'settingsOverworldPicker', levelPresets, presetPickerCallback );
    initializePicker ( 'settingsBonusPicker', levelPresets, presetPickerCallback );

    initializePicker ( 'settingsOverworldMusicPicker', musicPresets, musicPickerCallback );
    initializePicker ( 'settingsBonusMusicPicker', musicPresets, musicPickerCallback );

    initializePicker ( 'warpTypePicker', [ 'input', 'output' ], warpPickerCallback );

    var textFields = document.querySelectorAll ( '.number' );

    for ( var i = 0; i < textFields.length; ++i )
    {
        textFields [i].addEventListener ( 'input', function ( ev )
        {
            this.value = this.value.replace(/[^0-9.]/g, ''); 
            this.value = this.value.replace(/(\..*)\./g, '$1');

            if ( this.value.length >= 3 && parseInt ( this.value ) < 320 ) this.value = '320';
            if ( this.value.length == 4 && parseInt ( this.value ) > 4096 ) this.value = '4096';
        } );
    }

    document.getElementById ( 'createLevel' ).addEventListener ( 'click', function ( )
    {
        var overworldSize = parseInt ( document.querySelector ( '#overworldSize' ).value );
        var bonusSize = parseInt ( document.querySelector ( '#bonusSize' ).value );

        if ( isNaN ( overworldSize ) || isNaN ( bonusSize ) ) return;
        if ( overworldSize < 320 || bonusSize < 320 ) return;
        if ( overworldSize > 4096 || bonusSize > 4096 ) return;

        console.log ( 'creating level with properties' );
        console.log ( 'main width: ' + overworldSize );
        console.log ( 'bonus width: ' + bonusSize );

        marioeditor.createLevel ( overworldSize, currentPreset.overworld.theme, currentPreset.overworld.music, bonusSize, currentPreset.bonus.theme, currentPreset.bonus.music );
        
        exitOverlay ( );
    } );

    document.getElementById ( 'exitexport' ).addEventListener ( 'click', exitOverlay );
    document.getElementById ( 'cancelimport' ).addEventListener ( 'click', exitOverlay );

    document.getElementById ( 'runimport' ).addEventListener ( 'click', function ( )
    {
        var data = document.getElementById ( 'importarea' ).value;

        try 
        { 
            marioeditor.importLevel ( data );
            exitOverlay ( );
        } catch ( e )
        {
            alert ( 'Failed to import data: Invalid Level Format' );
        }
    } );

    document.getElementById ( 'saveLevel' ).addEventListener ( 'click', function ( )
    {
        marioeditor.project.overworld.background = currentPreset.overworld.theme;
        marioeditor.project.overworld.music = currentPreset.overworld.music;

        marioeditor.project.bonus.background = currentPreset.bonus.theme;
        marioeditor.project.bonus.music = currentPreset.bonus.music;

        marioeditor.editorActive = true;
        exitOverlay ( );
    } );

    document.getElementById ( 'newLevel' ).addEventListener ( 'click', function ( )
    {
        displayOverlay ( 'leveloptions' );
    } );

    document.getElementById ( 'selectexport' ).addEventListener ( 'click', function ( )
    {
        document.getElementById ( 'exportarea' ).select ( );
    } );

    document.getElementById ( 'exportLevel' ).addEventListener ( 'click', function ( )
    {
        document.getElementById ( 'exportarea' ).value = marioeditor.exportLevel ( );
        displayOverlay ( 'levelexport' );
    } );

    document.getElementById ( 'importLevel' ).addEventListener ( 'click', function ( )
    {
        displayOverlay ( 'importlevel' );
    } );

    document.getElementById ( 'cancelWarp' ).addEventListener ( 'click', function ( )
    {
        exitOverlay ( );
    } );

    document.getElementById ( 'saveWarp' ).addEventListener ( 'click', function ( )
    {
        var data = 
        {
            warpName: document.querySelector ( '#warpName' ).value,
            warpType: selectedWarpType,
            warpOutputName: document.querySelector ( '#warpOutputName' ).value
        };

        if ( data.warpName.length < 3 || data.warpName.length > 10 )
        {
            alert ( 'Warp name must have more than 3 characters and less than 11' );
            return;
        }

        exitOverlay ( );
        warpDialogCallback ( data );
    } );
}

function updateSettingsOverlay ( )
{
    updatePicker ( 'settingsOverworldPicker', marioeditor.project.overworld.background );
    updatePicker ( 'settingsBonusPicker', marioeditor.project.bonus.background );

    updatePicker ( 'settingsOverworldMusicPicker', marioeditor.project.overworld.music );
    updatePicker ( 'settingsBonusMusicPicker', marioeditor.project.bonus.music );
}

function clearOverlay ( )
{
    var layers = document.querySelectorAll ( '#overlay > .layer' );
    document.getElementById ( 'overlay' ).style.zIndex = -999;

    for ( var i = 0; i < layers.length; ++i ) 
        layers [i].style.display = 'none';
}

function displayOverlay ( layer )
{
    clearOverlay ( );
    var layer = document.querySelector ( '#overlay > #' + layer );
    document.getElementById ( 'overlay' ).style.zIndex = 999;

    if ( layer ) layer.style.display = 'block';
}

function displayWarpModal ( data, callback )
{
    marioeditor.editorActive = false;
    displayOverlay ( 'warpOptions' );

    warpDialogCallback = callback;
}